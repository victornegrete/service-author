#!/bin/bash

if [ -z "$GRADLE_HOME" ]; then
    echo "WARNING GRADLE_HOME is not set!"
    GRADLE_HOME=`locate gradle | grep "/bin/gradle$" | sort | tail -1 | sed 's/\/gradle$//'`
fi
if [ -z "$JAVA_HOME" ]; then
    echo "WARNING JAVA_HOME is not set!"
    JAVA_HOME=`locate java | grep "/bin/javac$" | sort | tail -1 | sed 's/\/javac$//'`
fi
PATH=$PATH:$GRADLE_HOME:$JAVA_HOME

PARENT_DIR=`pwd | tr '/' '\n' | tail -1`

if [ "$PARENT_DIR" == "scripts" ]; then
    pushd ..
fi

gradle clean shadowJar
cd target/libs
cp ../../src/main/resources/local-config.json .
java -cp .:service-article-1.0.0-fat.jar -jar ./service-article-1.0.0-fat.jar -conf local-config.json

if [ "$PARENT_DIR" == "scripts" ]; then
    popd
else
    cd ../..
fi
Article Service

Example usage:

curl -XPOST -H"Content-type: application/json; charset=utf-8" 'http://article-dev-main.aws.trendkite.com:80/article/extract' -d'{"urls":[{"url":"http://www.foo.com"}]}'

curl -XPOST -H"Content-type: application/json; charset=utf-8" 'http://article-dev-main.aws.trendkite.com:80/article/extract' -d'{"urls":[{"url":"http://www.foo.com"},{"url":"http://www.bar.com"}]}

Health check:

curl -XGET 'http://article-dev-main.aws.trendkite.com:80/health'
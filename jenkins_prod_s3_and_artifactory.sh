#!/bin/bash

export JAVA_HOME=/usr/java/latest
export PATH=$JAVA_HOME/bin:$PATH:/opt/gradle/latest/bin
export SERVICE_NAME="article"
echo "gradle version: " $(gradle -version)
export POM_VERSION=$(gradle getVersion | sed -n 1p)

clientJar="article-client-${POM_VERSION}.jar"

# Put relevant artifacts on s3
#     Service jar, configuration, run scripts
/usr/bin/aws s3 cp target/distributions/${SERVICE_NAME}*.tar.gz s3://tk-services/prod/${SERVICE_NAME}/current/

# Put relevant artifacts in artifactory
#     Clien jar
curl -XPUT --user admin:password "http://10.0.1.90:8081/artifactory/ext-release-local/com/trendkite/article-client/${POM_VERSION}/${clientJar}" --data-binary @target/${clientJar}



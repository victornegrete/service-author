package com.trendkite.api.client;

import com.trendkite.api.ArticleConstants;
import com.trendkite.api.model.ArticleRequest;
import com.trendkite.api.model.ArticleResponse;
import com.trendkite.client.TkServiceClient;
import com.trendkite.stats.StatsCollector;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class ArticleClient implements TkServiceClient {
    private static final Logger log = LoggerFactory.getLogger(ArticleClient.class);
    private static String svcName;
    // making client not static class-wide, so we can
    // talk to multiple servers if needs be
    private HttpClient client;
    private static JsonObject defaultConfig;
    private static Vertx vertx;

    private final StatsCollector statsCollector;

    public ArticleClient(JsonObject config, StatsCollector statsCollector) {
        this.statsCollector = statsCollector;
        JsonObject defaultConfig = new JsonObject();
        defaultConfig.put(BASECLIENT_OPTION_CONN_TIMEOUT, ArticleConstants.CLIENT_DEFAULT_CONNECTION_TIMEOUT);
        defaultConfig.put(BASECLIENT_OPTION_IDLE_TIMEOUT, ArticleConstants.CLIENT_DEFAULT_IDLE_TIMEOUT);
        initDefaults(ArticleConstants.SERVICE_APPLICATION_LABEL, defaultConfig);
        JsonObject configWeWant = config.copy();
        configWeWant.put(BASECLIENT_OPTION_HOST, ArticleConstants.getHostForClient(config));
        configWeWant.put(BASECLIENT_OPTION_PORT, ArticleConstants.getPortForClient(config));
        initClient(configWeWant);
    }

    public Future<ArticleResponse> extractArticles(ArticleRequest request) {
        Future<ArticleResponse> future = Future.future();
        getClient().post(ArticleConstants.SERVICE_URI)
                .putHeader(HttpHeaders.CONTENT_TYPE, ArticleConstants.RESPONSE_CONTENT_TYPE_JSON)
                .putHeader(HttpHeaders.ACCEPT, ArticleConstants.RESPONSE_CONTENT_TYPE_JSON)
                .handler(response -> {
                    if (response.statusCode() == HttpResponseStatus.OK.code()) {
                        log.info("Article extraction succeeded");
                        statsCollector.increment(ArticleConstants.METRIC_CLIENT_REQUESTS_OK);
                        response.bodyHandler(body -> {
                            ArticleResponse data = Json.decodeValue(body.toString("UTF-8"), ArticleResponse.class);
                            future.complete(data);
                        });
                    } else {
                        statsCollector.increment(ArticleConstants.METRIC_CLIENT_REQUESTS_FAILED);
                        response.bodyHandler(error -> {
                            String defaultMsg = "HTTP Response code="+response.statusCode()+", msg="+response.statusMessage();
                            log.error("Error: Article extraction failed: "+defaultMsg);
                            String failureMsg = defaultMsg;
                            try {
                                // There may or may not be a json body in the response, because
                                // if server is down the ELB may be responding.
                                if (error != null && !StringUtils.isBlank(error.toString())) {
                                    JsonObject errJson = error.toJsonObject();
                                    if (errJson != null) {
                                        failureMsg = failureMsg + ", error=" + errJson.getString(ArticleConstants.JSON_ENTITY_ERROR_PROPERTY, "");
                                    }
                                } else {
                                    log.error("Response was:"+Json.encodePrettily(response));
                                }
                            } finally {
                                future.fail(failureMsg);
                            }
                        });
                    }
                })
                .exceptionHandler(h -> {
                    String msg = h.getMessage();
                    future.fail(msg);
                })
                .end(Json.encode(request));
        return future;
    }

    public Future<JsonArray> healthResponse() {
        Future future = Future.future();
        getClient().get(TkServiceClient.HEALTH_ROUTE)
                .handler(response -> {
                    response.handler(body -> {
                        JsonArray result = body.toJsonArray();
                        future.complete(result);
                    });
                })
                .exceptionHandler(exc -> {
                    future.complete(new JsonArray());
                })
                .end();
        return future;
    }

    public ArticleResponse extractArticlesSync(ArticleRequest request) {
        CountDownLatch cl = new CountDownLatch(1);
        Future<ArticleResponse> future = extractArticles(request);
        final AtomicReference<ArticleResponse> response = new AtomicReference<>(null);
        future.setHandler(h -> {
            if (h.succeeded()) {
                response.set(h.result());
            } else {
                ArticleResponse r = new ArticleResponse();
                String msg = h.cause().getMessage();
                r.setError(msg);
                response.set(r);
            }
            cl.countDown();
        });
        // The connection already has timeouts, so we don't need them here.
        boolean finished = false;
        while (!finished) {
            try {
                finished = cl.await(500l, TimeUnit.MILLISECONDS);
            } catch (InterruptedException ie) {
                log.error("Ignored error: Interrupted sleep", ie);
            }
        }
        return response.get();
    }

    @Override
    public HttpClient getClient() {
        return client;
    }

    @Override
    public void setClient(HttpClient client) {
        this.client = client;
    }

    @Override
    public JsonObject getDefaultConfig() {
        return defaultConfig;
    }

    @Override
    public void setDefaultConfig(JsonObject defaultConfig) {
        this.defaultConfig = defaultConfig;
    }

    @Override
    public Logger getLog() {
        return log;
    }

    @Override
    public String getSvcName() {
        return svcName;
    }

    @Override
    public void setSvcName(String svcName) {
        this.svcName = svcName;
    }

    @Override
    public Vertx getVertx() {
        return vertx;
    }

    @Override
    public void setVertx(Vertx vertx) {
        this.vertx = vertx;
    }
}

package com.trendkite.api;

import io.vertx.core.json.JsonObject;

public class ArticleConstants {

    /*
     * SERVER SECTION
     */

    // Server Definition
    public static final String SERVICE_ID="article";
    public static final String SERVICE_APPLICATION_LABEL="service-"+SERVICE_ID;
    public static final int SERVICE_DEFAULT_PORT = 80;
    public static final String SERVICE_DEFAULT_HOST = "localhost";
    public static final String  SERVICE_URI = "/"+SERVICE_ID+"/extract";

    // Server Config
    public static final String JSON_DEPLOYMENT_CONFIG_PROPERTY="deployment";
    public static final String JSON_SERVICE_CONFIG_PROPERTY="service";
    public static final String JSON_SERVICE_PORT_CONFIG_PROPERTY ="http.port";
    public static final String JSON_DIFFBOT_CONFIG_PROPERTY="diffbot";
    public static final String JSON_AUTHOR_CONFIG_PROPERTY="author";
    public static final String JSON_DATE_CONFIG_PROPERTY="date";
    public static final String JSON_CONFIDENCE_CONFIG_PROPERTY="confidence.threshold";

    // Server Operations
    public static final String RESPONSE_CONTENT_TYPE_JSON = "application/json; charset=utf-8";
    public static final String JSON_ENTITY_ERROR_PROPERTY = "errorPayload";

    // Server Errors
    public static final String ERROR_URLS_NOT_FOUND = "No urls found";
    public static final String ERROR_INVALID_JSON = "Non-parseable JSON";
    public static final String ERROR_INCORRECT_JSON_REQUEST = "Invalid JSON request";
    public static final String ERROR_NULL_BODY = "Invalid null request";

    // Server Metrics
    public static final String METRIC_INCOMING_REQUESTS="requests";
    public static final String METRIC_INCOMING_INVALID_REQUESTS="requests-invalid";
    public static final String METRIC_FAILED_REQUESTS="requests-failed";
    public static final String METRIC_404_REQUESTS="requests-404";
    public static final String METRIC_INCOMING_URLS="incoming-urls";
    public static final String METRIC_SUCCESS_URLS="extracted-urls";
    public static final String METRIC_FAILED_URLS="failed-urls";
    public static final String METRIC_AUTHOR_DIFFBOTWINS="author-diffbot-wins";
    public static final String METRIC_AUTHOR_TKSERVICEWINS="author-tkservice-wins";
    public static final String METRIC_AUTHOR_NOTDETECTED="author-not-detected";
    public static final String METRIC_AUTHOR_MATCH="author-values-match";
    public static final String METRIC_DATE_DIFFBOTWINS="date-diffbot-wins";
    public static final String METRIC_DATE_TKSERVICEWINS="date-tkservice-wins";
    public static final String METRIC_DATE_NOTDETECTED="date-not-detected";
    public static final String METRIC_DATE_MATCH="date-values-match";
    public static final String METRIC_DIFFBOT_RESPONSE_TIME="diffbot-response-time";
    public static final String METRIC_AUTHOR_RESPONSE_TIME="author-response-time";
    public static final String METRIC_DATE_RESPONSE_TIME="date-response-time";

    // Server Configuration Handling
    private static JsonObject getDeploymentConfig(JsonObject topConfig) {
        JsonObject result = topConfig.containsKey(JSON_DEPLOYMENT_CONFIG_PROPERTY)
                ? topConfig.getJsonObject(JSON_DEPLOYMENT_CONFIG_PROPERTY)
                : new JsonObject();
        return result;
    }

    public static JsonObject getServiceConfig(JsonObject topConfig) {
        JsonObject deployConfig = getDeploymentConfig(topConfig);
        JsonObject result = deployConfig.containsKey(JSON_SERVICE_CONFIG_PROPERTY)
                ? deployConfig.getJsonObject(JSON_SERVICE_CONFIG_PROPERTY)
                : new JsonObject();
        return result;
    }

    public static JsonObject getComponentConfig(JsonObject serviceConfig, String sectionName) {
        JsonObject result = serviceConfig.containsKey(sectionName)
                ? serviceConfig.getJsonObject(sectionName)
                : new JsonObject();
        return result;
    }

    // Server Configuration Defaults
    public static int getPort(JsonObject serviceConfig) {
        // port for the service
        return serviceConfig.getInteger(JSON_SERVICE_PORT_CONFIG_PROPERTY, SERVICE_DEFAULT_PORT);
    }

    /*
     * CLIENT SECTION
     */

    // Client Config
    private static final String CLIENT_SERVICE_PREFIX=SERVICE_ID+".service.";
    private static final String CLIENT_CLIENT_PREFIX=SERVICE_ID+".client.";
    public static final String CLIENT_OPTION_HOST = CLIENT_SERVICE_PREFIX+"host";
    public static final String CLIENT_OPTION_PORT = CLIENT_SERVICE_PREFIX+"port";
    public static final String CLIENT_OPTION_CONNECTION_TIMEOUT = CLIENT_CLIENT_PREFIX+"connection.timeout";
    public static final String CLIENT_OPTION_IDLE_TIMEOUT = CLIENT_CLIENT_PREFIX+"idle.timeout";
    public static final String CLIENT_OPTION_ENV = CLIENT_CLIENT_PREFIX+"env";
    public static final int CLIENT_DEFAULT_CONNECTION_TIMEOUT = 2000;
    public static final int CLIENT_DEFAULT_IDLE_TIMEOUT = 2000;


    // Client metrics
    public static final String METRIC_CLIENT_REQUESTS_OK = "client-requests-ok";
    public static final String METRIC_CLIENT_REQUESTS_FAILED = "client-requests-failed";

    // Client Configuration Defaults
    public static String getHostForClient(JsonObject clientConfig) {
        if (clientConfig.containsKey(CLIENT_OPTION_HOST)) {
            return clientConfig.getString(CLIENT_OPTION_HOST);
        }
        if (clientConfig.containsKey(CLIENT_OPTION_ENV)) {
            String env = clientConfig.getString(CLIENT_OPTION_ENV);
            if (env.equalsIgnoreCase("dev") || env.equalsIgnoreCase("prod")) {
                // X-dev|prod-main.aws.trendkite.com
                return SERVICE_ID+"-"+env+"-main.aws.trendkite.com";
            }
        }
        return SERVICE_DEFAULT_HOST;
    }
    public static int getPortForClient(JsonObject clientConfig) {
        if (clientConfig.containsKey(CLIENT_OPTION_PORT)) {
            return clientConfig.getInteger(CLIENT_OPTION_PORT);
        }
        return SERVICE_DEFAULT_PORT;
    }
}

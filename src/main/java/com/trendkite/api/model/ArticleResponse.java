package com.trendkite.api.model;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;


public class ArticleResponse {

    private Map<String, ArticleExtracted> results;
    private String error;

    public ArticleResponse(Map<String, ArticleExtracted> articles) {
        this.results = articles;
        this.error = "";
    }

    public ArticleResponse() {
        results = new HashMap<>();
        this.error = "";
    }

    public Map<String, ArticleExtracted> getResults() { return results; }

    public void setResults(Map<String, ArticleExtracted> articles) { this.results = articles; }

    public void add(String url, ArticleExtracted article) {
        results.put(url, article);
    }

    public ArticleExtracted get(String url) {
        return results.get(url);
    }

    public String getError() { return error; }

    public void setError(String error) { this.error = error; }

    public boolean isError() { return !StringUtils.isEmpty(this.error); }


    public JsonObject convertToJson(Map<String, String> synonyms, String errorProperty, boolean allFields) {
        // synonyms: to -> from
        if (synonyms == null && StringUtils.isBlank(errorProperty)) {
            // no renaming needed
            return new JsonObject(Json.encode(this));
        }
        JsonObject result = new JsonObject();
        String errorPropertyName = StringUtils.isBlank(errorProperty) ? "error" : errorProperty;
        result.put(errorPropertyName, getError());
        if (allFields) {
            result.put("error", getError());
        }
        result.put("results", convertResponseMap(getResults(), synonyms, errorProperty, allFields));
        return result;
    }

    public JsonObject convertResponseMap(Map<String, ArticleExtracted> articleMap, Map<String, String> synonyms, String errorProperty, boolean allFields) {
        JsonObject result = new JsonObject();
        for (Map.Entry<String, ArticleExtracted> entry: articleMap.entrySet()) {
            String key = entry.getKey();
            ArticleExtracted a = entry.getValue();
            result.put(key, a.convertToJson(synonyms, errorProperty, allFields));
        }
        return result;
    }

    public JsonObject convertToJson() {
        return convertToJson(null, null);
    }

    public JsonObject convertToJson(Map<String, String> synonyms, String errorProperty) {
        return convertToJson(synonyms, errorProperty, true);
    }
}

package com.trendkite.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArticleExtracted {
    // this looks a lot like a diffbot article
    private String url;
    private String timestamp = "";
    private String icon = "";
    private String text = "";
    private String pageUrl = "";
    private String resolvedPageUrl = "";
    private String humanLanguage = "";
    private String date = ""; // millisecs since epoch, as string
    private String estimatedDate = "";
    private String type = "";
    private String author = "";
    private String authorUrl = "";
    private String title = "";
    private String html = "";
    private String publisherRegion = "";
    private String publisherCountry = "";
    private String siteName = "";
    private String publisher = "";
    private String errorMessage = "";
    private String errorCode = "";

    private ArticleTag[] tags = new ArticleTag[]{};
    private ArticleImage[] images = new ArticleImage[]{};

    public ArticleExtracted() {}

    public ArticleTag[] getTags() {
        return tags;
    }

    public void setTags(ArticleTag[] tags) {
        this.tags = tags;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPageUrl() { return pageUrl; }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getHumanLanguage() {
        return humanLanguage;
    }

    public void setHumanLanguage(String humanLanguage) {
        this.humanLanguage = humanLanguage;
    }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArticleImage[] getImages() {
        return images;
    }

    public void setImages(ArticleImage[] images) {
        this.images = images;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSiteName() { return siteName; }

    public void setSiteName(String siteName) { this.siteName = siteName; }

    public String getPublisherCountry() { return publisherCountry; }

    public void setPublisherCountry(String publisherCountry) { this.publisherCountry = publisherCountry; }

    public String getPublisherRegion() { return publisherRegion; }

    public void setPublisherRegion(String publisherRegion) { this.publisherRegion = publisherRegion; }

    public String getAuthorUrl() { return authorUrl; }

    public void setAuthorUrl(String authorUrl) { this.authorUrl = authorUrl; }

    public String getEstimatedDate() { return estimatedDate; }

    public void setEstimatedDate(String estimatedDate) { this.estimatedDate = estimatedDate; }

    public String getResolvedPageUrl() { return resolvedPageUrl; }

    public void setResolvedPageUrl(String resolvedPageUrl) { this.resolvedPageUrl = resolvedPageUrl; }

    public String getPublisher() { return publisher; }

    public void setPublisher(String publisher) { this.publisher = publisher; }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArticleExtracted)) return false;

        ArticleExtracted that = (ArticleExtracted) o;

        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;
        if (icon != null ? !icon.equals(that.icon) : that.icon != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (pageUrl != null ? !pageUrl.equals(that.pageUrl) : that.pageUrl != null) return false;
        if (resolvedPageUrl != null ? !resolvedPageUrl.equals(that.resolvedPageUrl) : that.resolvedPageUrl != null)
            return false;
        if (humanLanguage != null ? !humanLanguage.equals(that.humanLanguage) : that.humanLanguage != null)
            return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (estimatedDate != null ? !estimatedDate.equals(that.estimatedDate) : that.estimatedDate != null)
            return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (authorUrl != null ? !authorUrl.equals(that.authorUrl) : that.authorUrl != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (html != null ? !html.equals(that.html) : that.html != null) return false;
        if (publisherRegion != null ? !publisherRegion.equals(that.publisherRegion) : that.publisherRegion != null)
            return false;
        if (publisherCountry != null ? !publisherCountry.equals(that.publisherCountry) : that.publisherCountry != null)
            return false;
        if (siteName != null ? !siteName.equals(that.siteName) : that.siteName != null) return false;
        if (publisher != null ? !publisher.equals(that.publisher) : that.publisher != null) return false;
        if (errorMessage != null ? !errorMessage.equals(that.errorMessage) : that.errorMessage != null) return false;
        if (errorCode != null ? !errorCode.equals(that.errorCode) : that.errorCode != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(tags, that.tags)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(images, that.images);

    }

    @Override
    public int hashCode() {
        int result = url != null ? url.hashCode() : 0;
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (pageUrl != null ? pageUrl.hashCode() : 0);
        result = 31 * result + (resolvedPageUrl != null ? resolvedPageUrl.hashCode() : 0);
        result = 31 * result + (humanLanguage != null ? humanLanguage.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (estimatedDate != null ? estimatedDate.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (authorUrl != null ? authorUrl.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (html != null ? html.hashCode() : 0);
        result = 31 * result + (publisherRegion != null ? publisherRegion.hashCode() : 0);
        result = 31 * result + (publisherCountry != null ? publisherCountry.hashCode() : 0);
        result = 31 * result + (siteName != null ? siteName.hashCode() : 0);
        result = 31 * result + (publisher != null ? publisher.hashCode() : 0);
        result = 31 * result + (errorMessage != null ? errorMessage.hashCode() : 0);
        result = 31 * result + (errorCode != null ? errorCode.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(tags);
        result = 31 * result + Arrays.hashCode(images);
        return result;
    }

    // convenience
    public HttpResponseStatus convertToHttpError() {
        HttpResponseStatus status = null;
        if ( isError()) {
            try {
                status = new HttpResponseStatus(Integer.parseInt(this.errorCode), this.errorMessage);
            } catch (Exception e) {
                // ignore for now
            }
        }
        return status;
    }

    public boolean isError() {
        return ! StringUtils.isBlank(this.errorMessage) || ! StringUtils.isBlank(this.errorCode);
    }

    public JsonObject convertToJson(Map<String, String> synonyms, String errorProperty, boolean allFields) {
        // synonyms: to -> from
        // if allFields is false, then only the 'to' fields will appear in the result
        // if allFields is true, keep all fields as is, they may be overridden or new fields added  by synonyms
        if (synonyms == null && StringUtils.isBlank(errorProperty)) {
            // no renaming needed
            return new JsonObject(Json.encode(this));
        }
        JsonObject result;
        result = allFields ? new JsonObject(Json.encode(this)) : new JsonObject();

        String errorPropertyName = StringUtils.isBlank(errorProperty) ? "error" : errorProperty;
        String errorValue = isError() ? getErrorCode() + ": " + getErrorMessage() : "";
        result.put(errorProperty, errorValue);

        JsonObject article = new JsonObject(Json.encode(this));
        for (Map.Entry<String, String> entry: synonyms.entrySet()) {
            String to = entry.getKey();
            String from = entry.getValue();
            result.put(to, article.getValue(from));
        }
        return result;
    }
}

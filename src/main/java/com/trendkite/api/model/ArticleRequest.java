package com.trendkite.api.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ArticleRequest {
    private List<ArticleRequestUrl> urls;

    public ArticleRequest() { urls = new ArrayList<ArticleRequestUrl>(); }

    public ArticleRequest(List<ArticleRequestUrl> urls) {
        this.urls = new ArrayList<ArticleRequestUrl>(urls);
    }

    public ArticleRequest(String urlSingle) {
        urls = new ArrayList<>();
        urls.add(new ArticleRequestUrl(urlSingle));
    }

    public List<ArticleRequestUrl> getUrls() {
        return urls;
    }

    public List<String> getStringUrls() {
        return getUrls().stream().map(i -> i.getUrl()).collect(Collectors.toList());
    }

    public void setUrls(List<ArticleRequestUrl> urls) {
        this.urls = urls;
    }

}
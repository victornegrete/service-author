package com.trendkite.api.model;

public class ArticleRequestUrl {
    String url;

    public ArticleRequestUrl() { }

    public ArticleRequestUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleRequestUrl that = (ArticleRequestUrl) o;
        return url != null ? !url.equals(that.url) : that.url != null;
    }

    @Override
    public int hashCode() {
        int result = url != null ? url.hashCode() : 0;
        return result;
    }
}
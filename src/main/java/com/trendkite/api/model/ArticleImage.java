package com.trendkite.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArticleImage {
    private String title = "";
    private long naturalHeight;
    private boolean primary;
    private long naturalWidth;
    private String url = "";

    public ArticleImage() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getNaturalHeight() {
        return naturalHeight;
    }

    public void setNaturalHeight(long naturalHeight) {
        this.naturalHeight = naturalHeight;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public long getNaturalWidth() {
        return naturalWidth;
    }

    public void setNaturalWidth(long naturalWidth) {
        this.naturalWidth = naturalWidth;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
};

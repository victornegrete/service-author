package com.trendkite.service;

import com.trendkite.api.model.ArticleExtracted;
import com.trendkite.api.model.ArticleRequestUrl;
import com.trendkite.api.model.ArticleResponse;
import com.trendkite.service.extraction.ContentExtractor;
import com.trendkite.service.stats.StatsHandler;
import com.trendkite.service.stats.StatsHelper;
import com.trendkite.stats.datadog.DataDogStatsCollector;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.trendkite.api.ArticleConstants.*;

public class ArticleService extends AbstractVerticle implements HealthCheckVerticle {
    private static final Logger log = LoggerFactory.getLogger(ArticleService.class);

    public static DataDogStatsCollector statsCollector;

    private JsonObject config; // a service config
    private ContentExtractor extractor;

    @Override
    public void start(Future<Void> startFuture) throws IOException {
        log.info("Article service start() vertx="+vertx+", this="+this);
        log.info("also from service start, loader="+this.getClass().getClassLoader());
        // config() returns the content of "service" section of the file named in the -conf argument
        log.info("Article service config="+config().toString());
        // get configuration and initialize dependencies for the service.
        initServices(); // initializes local config variable
        int port = getPort(config);
        log.info("Starting vertx server "+SERVICE_APPLICATION_LABEL+" on port " + port);

        Router router = Router.router(vertx);
        router.route()
                .handler(BodyHandler.create()); // put this up top or else vertx will be mad

        router.post(SERVICE_URI)
                .consumes(PRODUCES_APPLICATION_JSON)
                .produces(PRODUCES_APPLICATION_JSON)
                .handler(this::extractArticles);

        // Add health check
        this.addHealthCheck(router, vertx, getServiceHealthCheckFeatures());

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(port, result -> {
                    if (result.succeeded()) {
                        startFuture.complete();
                    } else {
                        startFuture.fail(result.cause());
                    }
                });
    }

    public ArrayList<ServiceFeature> getServiceHealthCheckFeatures() {
        return extractor.getServiceHealthCheckFeatures();
    }

    public void initServices() {
        config = (config() == null) ? new JsonObject() : config();
        String mode = config.getString("env", "local");
        log.info("Running in "+mode+" mode");
        statsCollector = StatsHandler.getCollector(config, Collections.<String>emptyList());
        extractor = new ContentExtractor(vertx, config, statsCollector);
    }

    public void extractArticles(RoutingContext routingContext) {
        // Getting request payload
        Buffer buffer = routingContext.getBody();
        JsonObject requestPayload = null;
        if (buffer == null ) {
            sendErrorResponse(ERROR_NULL_BODY, HttpResponseStatus.BAD_REQUEST, routingContext);
            return;
        } else {
            try {
                requestPayload = buffer.toJsonObject();
            } catch (Exception e) {
                // not parseable json
                sendErrorResponse(ERROR_INVALID_JSON, HttpResponseStatus.BAD_REQUEST, routingContext, e);
                return;
            }
        }
        processRequest(requestPayload, routingContext);
    }


    public Future waitAll(List<Future> fs) {
        Future result = Future.future();
        final AtomicInteger count = new AtomicInteger(0);
        final int expected = fs.size();
        for (Future f: fs) {
            f.setHandler( h -> {
                if (count.incrementAndGet() >= expected) {
                    result.complete(count);
                }
            });
        }
        return result;
    }

    public void processRequest(JsonObject request, RoutingContext routingContext) {
        statsCollector.increment(METRIC_INCOMING_REQUESTS);
        List<ArticleRequestUrl> urls = null;
        try {
            urls = getUrls(request);
        } catch (Exception e) {
            // invalid request
            sendErrorResponse(ERROR_INCORRECT_JSON_REQUEST, HttpResponseStatus.BAD_REQUEST, routingContext, e);
            statsCollector.increment(METRIC_INCOMING_INVALID_REQUESTS);
            return;
        }
        if (urls == null || urls.isEmpty()) {
            statsCollector.increment(METRIC_404_REQUESTS);
            sendErrorResponse(ERROR_URLS_NOT_FOUND, HttpResponseStatus.NOT_FOUND, routingContext);
            return;
        }
        // Got some urls
        int totalUrls = urls.size();
        log.debug("Got "+totalUrls+" urls for processing");
        statsCollector.adjustCount(METRIC_INCOMING_URLS, totalUrls);
        ArticleResponse response = new ArticleResponse();
        List<Future> articleFutures = extractor.extract(urls);
        waitAll(articleFutures).setHandler( h -> {
            int ok = 0;
            int failed = 0;
            int withErrors = 0;
            for (Future f : articleFutures) {
                if (f.succeeded()) {
                    ArticleExtracted result = (ArticleExtracted) f.result();
                    if (result.isError()) {
                        withErrors ++;
                        response.add(result.getUrl(), result);
                    }
                    else {
                        ok ++;
                        response.add(result.getPageUrl(), result);
                    }
                } else {
                    failed ++;
                }
            }
            statsCollector.adjustCount(METRIC_SUCCESS_URLS, ok);
            statsCollector.adjustCount(METRIC_FAILED_URLS, failed + withErrors);
            if (ok + withErrors > 0) {
                // Per article errors are useful to the caller
                StatsHelper.incrementAndSetSuccessRequestWithJsonPayload(routingContext, Json.encode(response));
            } else {
                statsCollector.increment(METRIC_FAILED_REQUESTS);
                String err = "Failed to extract content for "+failed+" urls (out of "+totalUrls+")";
                sendErrorResponse(err, HttpResponseStatus.INTERNAL_SERVER_ERROR, routingContext);
            }
        });
    }

    public List<ArticleRequestUrl> getUrls(JsonObject requestPayload) {
        List<ArticleRequestUrl> payload = new ArrayList<>();
        if (requestPayload == null) {
            // incoming request was invalid (null)
            return payload;
        }
        JsonArray urls = requestPayload.getJsonArray("urls");
        if (urls == null || urls.isEmpty()) {
            // incoming request malformed or empty
            return payload;
        }
        Iterator it = urls.iterator();
        while(it.hasNext()) {
            JsonObject url = (JsonObject)it.next();
            ArticleRequestUrl tmp = new ArticleRequestUrl();
            String urlString = url.getString("url", "");
            if (! StringUtils.isEmpty(urlString)) {
                tmp.setUrl(urlString);
                payload.add(tmp);
            } else {
                log.info("Invalid empty incoming url, ignored");
            }
        }
        return payload;
    }

    public void sendErrorResponse (String description, HttpResponseStatus httpStatus, RoutingContext routingContext, Throwable e) {
        log.info(description);
        String msg = (!StringUtils.isEmpty(description)) ? description: httpStatus.toString();
        String payload = (e!=null) ? msg+ ": "+ e.getMessage() : msg;;
        StatsHelper.incrementAndSetClientRequestError(routingContext, payload, true, httpStatus);
    }

    public void sendErrorResponse (String description, HttpResponseStatus httpStatus, RoutingContext routingContext) {
        sendErrorResponse(description, httpStatus, routingContext, null);
    }

}

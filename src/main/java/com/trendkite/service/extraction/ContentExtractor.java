package com.trendkite.service.extraction;

import com.trendkite.api.ArticleConstants;
import com.trendkite.api.client.AuthorClient;
import com.trendkite.api.client.DateClient;
import com.trendkite.api.model.*;
import com.trendkite.client.TkServiceClient;
import com.trendkite.service.ServiceFeature;
import com.trendkite.stats.StatsCollector;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.trendkite.api.ArticleConstants.*;

public class ContentExtractor {
    private Logger log = LoggerFactory.getLogger(ContentExtractor.class);
    private final Vertx vertx;
    private final JsonObject config;
    private final DiffbotClient diffbotClient;
    private final AuthorClient authorClient; // has static http client
    private final DateClient dateClient; // has static http client
    public static StatsCollector statsCollector;
    private final double DEFAULT_AUTHOR_CONFIDENCE_THRESHOLD = 51.0; // 51%, values in(0-100)
    private final double DEFAULT_DATE_CONFIDENCE_THRESHOLD = 0.51; // 51%, values in (0-1)
    private final double AUTHOR_CONFIDENCE_THRESHOLD;
    private final double DATE_CONFIDENCE_THRESHOLD;
    private final String ENABLED_PROPERTY="enabled";
    private final boolean authorEnabled;
    private final boolean dateEnabled;
    private final List<String> BLACKLIST_EXTENSIONS = Arrays.asList(".pdf");

    private static final DateTimeFormatter RFC1123_DATE_TIME_FORMATTER = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'");

    public ContentExtractor(Vertx vertx, JsonObject serviceConfig, StatsCollector statsCollector) {
        this.vertx = vertx;
        this.config = serviceConfig;
        this.statsCollector = statsCollector;

        JsonObject diffbotConfig = getComponentConfig(config, JSON_DIFFBOT_CONFIG_PROPERTY);
        log.info("ContextExtractor DiffbotClient config="+diffbotConfig.toString());
        diffbotClient = new DiffbotClient(vertx, diffbotConfig, statsCollector); // could be a different extractor in the future

        JsonObject authorConfig = getComponentConfig(config, JSON_AUTHOR_CONFIG_PROPERTY);
        log.info("ContextExtractor AuthorClient config="+authorConfig.toString());
        authorClient = new AuthorClient(authorConfig);
        authorEnabled = authorConfig.getBoolean(ENABLED_PROPERTY, true);
        AUTHOR_CONFIDENCE_THRESHOLD = authorConfig.getDouble(JSON_CONFIDENCE_CONFIG_PROPERTY, DEFAULT_AUTHOR_CONFIDENCE_THRESHOLD);

        JsonObject dateConfig = getComponentConfig(config, JSON_DATE_CONFIG_PROPERTY);
        log.info("ContextExtractor DateClient config="+dateConfig.toString());
        dateClient = new DateClient(dateConfig);
        dateEnabled = dateConfig.getBoolean(ENABLED_PROPERTY, true);
        DATE_CONFIDENCE_THRESHOLD = dateConfig.getDouble(JSON_CONFIDENCE_CONFIG_PROPERTY, DEFAULT_DATE_CONFIDENCE_THRESHOLD);

    }

    private ServiceFeature getSubordinateServiceFeature(TkServiceClient subordinateClient, String subordinateLabel) {
        return new ServiceFeature() {
            @Override
            public Future<ServiceFeature.Result> runHealthCheck() {
                Future<ServiceFeature.Result> future = Future.future();
                boolean status = subordinateClient.health();
                ServiceFeature.Result result = new ServiceFeature.Result(
                        subordinateLabel,
                        status ? ServiceFeature.ResultStatus.OK : ServiceFeature.ResultStatus.BAD);
                future.complete(result);
                return future;
            }
        };
    }

    private ServiceFeature getAuthorServiceFeature() {
        return getSubordinateServiceFeature(authorClient, "subordinate-author-service");
    }

    private ServiceFeature getDateServiceFeature() {
        return getSubordinateServiceFeature(dateClient, "subordinate-date-service");
    }

    private ServiceFeature getDiffbotServiceFeature() {
        // diffbotClient is not a TkServiceClient
        return new ServiceFeature() {
            @Override
            public Future<ServiceFeature.Result> runHealthCheck() {
                Future<ServiceFeature.Result> future = Future.future();
                boolean status = diffbotClient.health();
                ServiceFeature.Result result = new ServiceFeature.Result(
                        "subordinate-diffbot-service",
                        status ? ServiceFeature.ResultStatus.OK : ServiceFeature.ResultStatus.BAD);
                future.complete(result);
                return future;
            }
       };
    }

    public ArrayList<ServiceFeature> getServiceHealthCheckFeatures() {
        ArrayList<ServiceFeature> features = new ArrayList<ServiceFeature>();
        features.add(getAuthorServiceFeature());
        features.add(getDateServiceFeature());
        features.add(getDiffbotServiceFeature());
        return features;
    }

    public ArticleExtracted blackListed(String url) {
        // If blacklisted, returns an error ArticleExtracted
        for (String ending: BLACKLIST_EXTENSIONS) {
            if (url.endsWith(ending)) {
                ArticleExtracted a = new ArticleExtracted();
                a.setUrl(url);
                a.setErrorCode(Integer.toString(HttpResponseStatus.NOT_IMPLEMENTED.code()));
                a.setErrorMessage("Content extraction of url='"+url+"' not supported: extension '"+ending+"' is blacklisted");
                return a;
            }
        }
        return null;
    }

    public List<Future> extractWITHAUTHORNDATE(List<ArticleRequestUrl> urls) {
        List<Future> futures = new ArrayList<>();
        for (ArticleRequestUrl url: urls ) {
            String sUrl = url.getUrl();
            ArticleExtracted bla = blackListed(sUrl);
            if (bla != null) {
                Future<ArticleExtracted> blackListedFuture = Future.future();
                blackListedFuture.complete(bla);
                futures.add(blackListedFuture);
            }
            else {
                Future<ArticleExtracted> diffbotFuture = getContent(sUrl);
                Future<ArticleExtracted> authorFuture = getAuthor(diffbotFuture, sUrl);
                Future<ArticleExtracted> dateFuture = getDate(authorFuture, sUrl);
                futures.add(dateFuture);
            }
        }
        return futures;
    }

    public List<Future> extract(List<ArticleRequestUrl> urls) {
        List<Future> futures = new ArrayList<>();
        for (ArticleRequestUrl url: urls ) {
            String sUrl = url.getUrl();
            ArticleExtracted bla = blackListed(sUrl);
            if (bla != null) {
                Future<ArticleExtracted> blackListedFuture = Future.future();
                blackListedFuture.complete(bla);
                futures.add(blackListedFuture);
            }
            else {
                Future<ArticleExtracted> diffbotFuture = getContent(sUrl);
                futures.add(diffbotFuture);
            }
        }
        return futures;
    }

    public Future<ArticleExtracted> getContent (String url) {
        return diffbotClient.fetch(url);
    }

    public AuthorDetectRequest createAuthorRequest(ArticleExtracted a) {
        AuthorDetectRequest req = new AuthorDetectRequest();
        req.setUrl(a.getPageUrl());
        req.setContent(a.getHtml());
        req.setLang(a.getHumanLanguage());
        return req;
    }

    public DateExtractionRequest createDateRequest(ArticleExtracted a) {
        DateExtractionRequest req = new DateExtractionRequest();
        req.setUrl(a.getPageUrl());
        req.setContent(a.getHtml());
        req.setLang(a.getHumanLanguage());
        return req;
    }

    private Long getSvcDateMillis(Date dateDate) {
        // Our current date-extraction service returns a Date object basically
        // with the time-from-epoch. We want to normalize
        if (dateDate == null) {
            return null;
        }
        DateTime dateTime = new DateTime(dateDate, DateTimeZone.UTC);
        return dateTime.getMillis();
    }

    private boolean isSameDate(Long diffbotDateMillis, Long inDateMillis) {
        // We are going to ignore the hours part of the date and call it a match
        // if the date part matches
        if (inDateMillis == null) {
            return false;
        }
        DateTime diffbotDate = new DateTime(diffbotDateMillis);
        LocalDate diffbotDateOnly = diffbotDate.toLocalDate();
        DateTime dateDate = new DateTime(inDateMillis);
        LocalDate dateDateOnly = dateDate.toLocalDate();
        return (diffbotDateOnly.compareTo(dateDateOnly) == 0);
    }

    public Future<ArticleExtracted> getAuthor (Future<ArticleExtracted> priorFuture, String url)
    {
        Future<ArticleExtracted> future = Future.future();
        String urlSuffix = ", url='"+ url + "'";
        priorFuture.setHandler( dH -> {
            if (dH.succeeded() && dH.result() != null) {
                ArticleExtracted a = dH.result();
                if (! a.isError()) {
                    if (authorEnabled) {
                        if (!StringUtils.isBlank(a.getText())) {
                            long startTime = System.currentTimeMillis();
                            AuthorDetectRequest req = createAuthorRequest(a);
                            Future<AuthorDetectResponse> authorFuture = authorClient.detectAuthor(req);
                            authorFuture.setHandler(aH -> {
                                long responseTime = System.currentTimeMillis() - startTime;
                                log.info("Author response time: " + responseTime + "ms"+urlSuffix);
                                statsCollector.time(ArticleConstants.METRIC_AUTHOR_RESPONSE_TIME, responseTime);
                                AuthorDetectResponse response = (aH.succeeded()) ? aH.result() : null;
                                String authorAuthor = (response == null || response.getAuthor() == null) ? "" : response.getAuthor();
                                double authorConfidence = (response == null) ? 0.0 : response.getConfidence();
                                if (a.getAuthor() == null) {
                                    //just being defensive
                                    a.setAuthor("");
                                }
                                String diffbotAuthor = a.getAuthor();
                                if (diffbotAuthor.equalsIgnoreCase(authorAuthor)) {
                                    log.info("Obtained author '" + authorAuthor + "' with confidence " + authorConfidence + " matching diffbot"+urlSuffix);
                                    statsCollector.increment(StringUtils.isEmpty(authorAuthor) ? METRIC_AUTHOR_NOTDETECTED : METRIC_AUTHOR_MATCH);
                                } else if (authorConfidence >= AUTHOR_CONFIDENCE_THRESHOLD) {
                                    log.info("Obtained author '" + authorAuthor + "' with confidence " + authorConfidence + " over threshold[" + AUTHOR_CONFIDENCE_THRESHOLD + "]. Would accept, though differs from diffbot's '" + diffbotAuthor + "'"+urlSuffix);
                                    statsCollector.increment(METRIC_AUTHOR_TKSERVICEWINS);
                                } else {
                                    log.info("Obtained author '" + authorAuthor + "' with confidence " + authorConfidence + " under threshold[" + AUTHOR_CONFIDENCE_THRESHOLD + "]. Rejected, took diffbot's '" + diffbotAuthor + "'"+urlSuffix);
                                    statsCollector.increment(METRIC_AUTHOR_DIFFBOTWINS);
                                }
                                future.complete(a);
                            });
                        } else {
                            log.info("Author service not called on empty content"+urlSuffix);
                            future.complete(a);
                        }
                    } else {
                        log.info("Author service is disabled");
                        future.complete(a);
                    }
                } else {
                    // do not attempt to get author of an error article
                    String msg = "Skipping author processing on error object: " + a.convertToHttpError() + urlSuffix;
                    log.info(msg);
                    future.complete(a);
                }
            } else if (dH.failed()) {
                String msg = "Failed diffbotClient, skipping further processing";
                log.error(msg + urlSuffix, dH.cause());
                future.fail(msg+": "+dH.cause());
            } else {
                // succeeded but found no article
                String msg = "Unexpected null result from diffbotClient, skipping further processing";
                log.error(msg + urlSuffix);
                future.fail(msg);
            }
        });
        return future;
    }

    public Future<ArticleExtracted> getDate (Future<ArticleExtracted> priorFuture, String url)
    {
        Future<ArticleExtracted> future = Future.future();
        String urlSuffix = ", url='"+ url + "'";
        priorFuture.setHandler( dH -> {
            if (dH.succeeded() && dH.result() != null) {
                ArticleExtracted a = dH.result();
                if (! a.isError()) {
                    if (dateEnabled) {
                        if (!StringUtils.isBlank(a.getText())) {
                            long startTime = System.currentTimeMillis();
                            DateExtractionRequest req = createDateRequest(a);
                            Future<DateExtractionResponse> dateFuture = dateClient.extractDates(req);
                            dateFuture.setHandler(aH -> {
                                long responseTime = System.currentTimeMillis() - startTime;
                                log.info("Date response time: " + responseTime + "ms"+urlSuffix);
                                statsCollector.time(ArticleConstants.METRIC_DATE_RESPONSE_TIME, responseTime);
                                DateExtractionResponse response = (aH.succeeded()) ? aH.result() : null;
                                Date origDateDate = (response == null || response.getPublishDate() == null) ? null : response.getPublishDate();
                                Long dateDateMillis = getSvcDateMillis(origDateDate);
                                double dateConfidence = (response == null || StringUtils.isEmpty(response.getConfidence())) ? 0.0 : Double.parseDouble(response.getConfidence());
                                Long diffbotDateMillis = (StringUtils.isEmpty(a.getDate())) ? null : Long.parseLong(a.getDate()); // millis since epoch
                                if (isSameDate(diffbotDateMillis, dateDateMillis)) {
                                    log.info("Obtained date '" + dateDateMillis + "' with confidence " + dateConfidence + " matching diffbot"+urlSuffix);
                                    statsCollector.increment(dateDateMillis == null ? METRIC_DATE_NOTDETECTED : METRIC_DATE_MATCH);
                                } else if (dateDateMillis == null) {
                                    if (dateConfidence >= DATE_CONFIDENCE_THRESHOLD) {
                                        log.info("Obtained date '" + dateDateMillis + "' with confidence " + dateConfidence + " over threshold[" + DATE_CONFIDENCE_THRESHOLD + "]. Would accept, though differs from diffbot's '" + diffbotDateMillis + "'"+urlSuffix);
                                        statsCollector.increment(METRIC_DATE_TKSERVICEWINS);
                                    } else {
                                        log.info("Obtained date '" + dateDateMillis + "' with confidence " + dateConfidence + " under threshold[" + DATE_CONFIDENCE_THRESHOLD + "]. Rejected, took diffbot's '" + diffbotDateMillis + "'"+urlSuffix);
                                        statsCollector.increment(METRIC_DATE_DIFFBOTWINS);
                                    }
                                } else {
                                    // dateDateMillis is empty and diffbot's is not
                                    log.info("Obtained empty date '" + dateDateMillis + "' with confidence " + dateConfidence + ". Rejected, took diffbot's '" + diffbotDateMillis + "'"+urlSuffix);
                                    statsCollector.increment(METRIC_DATE_NOTDETECTED);
                                }
                                future.complete(a);
                            });
                        } else {
                            log.info("Date service not called on empty content" + urlSuffix);
                            future.complete(a);
                        }
                    } else {
                        log.info("Date service is disabled");
                        future.complete(a);
                    }
                } else {
                    // do not attempt to get date of an error article
                    String msg = "Skipping date processing on error object: " + a.convertToHttpError()+urlSuffix;
                    log.info(msg);
                    future.complete(a);
                }
            } else {
                String msg = "Failed authorClient, skipping further processing";
                log.error(msg+urlSuffix, dH.cause());
                future.fail(msg+": "+dH.cause());
            }
        });
        return future;
    }
}


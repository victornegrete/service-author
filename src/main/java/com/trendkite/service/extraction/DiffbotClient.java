package com.trendkite.service.extraction;


import com.trendkite.api.ArticleConstants;
import com.trendkite.api.model.ArticleExtracted;
import com.trendkite.client.TkServiceClient;
import com.trendkite.stats.StatsCollector;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.atomic.AtomicInteger;

public class DiffbotClient extends DiffbotConstants {
    private static final Logger log = LoggerFactory.getLogger(DiffbotClient.class);
    private final StatsCollector statsCollector;
    private HttpClient client; // must not be static to be thread safe
    private static String apiKey;
    private static String diffbotEndpoint;
    private static int timeoutInMillis;
    private static boolean enabled;
    private static int retries;
    private static final DateTimeFormatter RFC1123_DATE_TIME_FORMATTER = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'");
    private static final String DIFFBOT_ERROR_CODE_FIELD="errorCode";
    private static final String DIFFBOT_ERROR_MSG_FIELD="error";
    private enum HealthStatusType { STATUS_NOTREACHED_YET, STATUS_REACHED};
    private HealthStatusType healthStatus;
    private Vertx vertx;

    public DiffbotClient(Vertx vertx, JsonObject config, StatsCollector statsCollector) {
        // cached property values for speed and convenience
        enabled = config.getBoolean(PROP_DIFFBOT_ENABLED, DEFAULT_DIFFBOT_ENABLED);
        apiKey = config.getString(PROP_DIFFBOT_API_KEY, DEFAULT_DIFFBOT_API_KEY);
        diffbotEndpoint = computeEndpoint(config.getString(PROP_DIFFBOT_API_VERSION, DEFAULT_DIFFBOT_API_VERSION));
        timeoutInMillis = config.getInteger(PROP_DIFFBOT_ARTICLE_TIMEOUT, DEFAULT_DIFFBOT_ARTICLE_TIMEOUT);
        retries = config.getInteger(PROP_DIFFBOT_RETRIES, DEFAULT_DIFFBOT_RETRIES);
        this.statsCollector = statsCollector;
        this.vertx = vertx;
        initClient(config);
    }

    public boolean health() {
        // we can't really tell unless we try and get a response
        return (healthStatus == HealthStatusType.STATUS_REACHED);
    }

    public Future<ArticleExtracted> fetch(String url) {
        Future<ArticleExtracted> future = Future.future();
        AtomicInteger count = new AtomicInteger(retries);
        Future<ArticleExtracted> next = fetchWithRetries(url, count);
        next.setHandler(delegatorHandler(future));
        return future;
    }

    static private Handler<AsyncResult<ArticleExtracted>> delegatorHandler(Future<ArticleExtracted> future) {
        return h -> {
            if (h.succeeded()) {
                future.complete(h.result());
            } else  {
                future.fail(h.cause());
            }
        };
    }

    public Future<ArticleExtracted> fetchWithRetries(String url, AtomicInteger retryCount) {
        Future<ArticleExtracted> future = Future.future();
        Future<ArticleExtracted> trial = fetchOnce(url, retryCount.get());
        trial.setHandler( h -> {
            if (h.succeeded()) {
                ArticleExtracted a = h.result();
                if (a.isError()) {
                    future.complete(a);
                } else {
                    if (!StringUtils.isBlank(a.getText())) {
                        future.complete(a);
                    } else {
                        // will retry on empty text
                        int left = retryCount.decrementAndGet();
                        if (left == 0) {
                            log.info("Exhausted "+retries+" retries to fetch article '" + url + '"');
                            future.complete(a);
                        } else {
                            Future<ArticleExtracted> retryFuture = fetchWithRetries(url, retryCount);
                            retryFuture.setHandler(delegatorHandler(future));
                        }
                    }
                }
            } else {
                future.fail(h.cause());
            }
        });
        return future;
    }

    public Future<ArticleExtracted> fetchOnce(String url, int trialNumber) {
        Future<ArticleExtracted> future = Future.future();
        long startTime = System.currentTimeMillis();

        if (! enabled ) {
            log.info("Diffbot context extraction disabled");
            future.complete();
            return future;
        }

        try {
            String uriForLog="[url='"+url+"', trial="+trialNumber+"] ";
            String uri = computeUri(diffbotEndpoint, apiKey, url, timeoutInMillis);
            log.debug(uriForLog+"Calling diffbot: "+uri+", client="+client);
            statsCollector.increment(METRIC_DIFFBOT_API_CALLS);
            client.get(uri)
                    .putHeader(HttpHeaders.ACCEPT, MEDIATYPE_APPLICATION_JSON)
                    .putHeader(HEADER_DIFFBOT_REFERER, DEFAULT_REFERER_HEADER)
                    .handler(response -> {
                        long responseTime = System.currentTimeMillis() - startTime;
                        log.info(uriForLog+"Diffbot response time: " + responseTime + "ms");
                        statsCollector.time(ArticleConstants.METRIC_DIFFBOT_RESPONSE_TIME, responseTime);
                        if (response.statusCode() == HttpResponseStatus.OK.code()) {
                            response.bodyHandler(body -> {
                                JsonObject result = body.toJsonObject();
                                ArticleExtracted article = createResponse(url, result);
                                if (article != null) {
                                    // could be an error, but we are only counting successful round trips to diffbot
                                    statsCollector.increment(METRIC_DIFFBOT_DATA_SUCCESS);
                                    future.complete(article);
                                } else {
                                    statsCollector.increment(METRIC_DIFFBOT_UNEXPECTED_RESPONSE);
                                    future.fail("Unexpected diffbot response: unable to properly parse JSON response. Check logs");
                                }
                            });
                        } else {
                            statsCollector.increment(METRIC_DIFFBOT_RESPONSE_FAILURE);
                            String msg = uriForLog+"Request to diffbot failed with [" + response.statusCode() + "] : " + response.statusMessage();
                            log.error(msg);
                            response.bodyHandler(error -> {
                                String sErr = error.toString();
                                future.fail(msg+": "+sErr);
                            });

                        }
                    })
                    .exceptionHandler(result -> {
                        statsCollector.increment(METRIC_DIFFBOT_RESPONSE_EXCEPTION);
                        log.error(uriForLog+"Diffbot request got an exception", result);
                        future.fail(result);
                    })
                    .end();
        } catch (UnsupportedEncodingException e) {
            statsCollector.increment(METRIC_DIFFBOT_UNENCODEABLE_URLS);
            log.error("Unable to encode url to send to diffbot");
            future.fail(e);
        }
        return future;
    }

    private Long millisSinceEpoch(String sDiffbotDate) {
        // Diffbot claims its dates are encoded per RFC 1123 (HTTP/1.1)
        // We are going to turn it to millis since epoch
        if (StringUtils.isEmpty(sDiffbotDate)) {
            return null;
        }
        DateTime diffbotDate =  RFC1123_DATE_TIME_FORMATTER.parseDateTime(sDiffbotDate);
        return diffbotDate.getMillis();
    }

    public ArticleExtracted createResponse(String url, JsonObject diffbotResponse) {
        // third diffbot parser!
        // see DiffbotArticlesSource.java
        // see DiffbotArticleExtractor.java
        try {
            JsonArray jArticles = diffbotResponse.getJsonArray("objects");
            int numArticles = jArticles.size();
            if (numArticles == 1) {
                ArticleExtracted a = Json.decodeValue(jArticles.getJsonObject(0).toString(), ArticleExtracted.class);
                Long millis = millisSinceEpoch(a.getDate());
                a.setDate((millis == null) ? "": String.valueOf(millis));
                a.setUrl(url);
                healthStatus = HealthStatusType.STATUS_REACHED;
                return a;
            }
            log.error("Expected 1 article, got "+ numArticles);
        } catch (Exception e) {
            // extract error message, if available
            try {
                int errorCode = -1;
                String errorMsg = "";
                if (diffbotResponse.containsKey(DIFFBOT_ERROR_CODE_FIELD)) {
                    errorCode = diffbotResponse.getInteger(DIFFBOT_ERROR_CODE_FIELD);
                }
                if (diffbotResponse.containsKey(DIFFBOT_ERROR_MSG_FIELD)) {
                    errorMsg = diffbotResponse.getString(DIFFBOT_ERROR_MSG_FIELD);
                }
                if (errorCode != -1) {
                    ArticleExtracted a = new ArticleExtracted();
                    a.setErrorCode(String.valueOf(errorCode));
                    a.setErrorMessage(errorMsg);
                    a.setUrl(url);
                    healthStatus = HealthStatusType.STATUS_REACHED;
                    return a;
                } else {
                    log.error("Exception parsing diffbot article api response", e);
                }
            } catch (Exception ee) {
                log.error("Exception parsing diffbot article api response", ee);
            }
        }
        return null;
    }

    public void initClient(JsonObject config) {
        // called once per DiffbotClient instance
        HttpClientOptions options = new HttpClientOptions();
        // connect stuff
        options.setIdleTimeout(config.getInteger(PROP_DIFFBOT_SOCKET_TIMEOUT, DEFAULT_DIFFBOT_SOCKET_TIMEOUT));
        options.setConnectTimeout(config.getInteger(PROP_DIFFBOT_CONNECT_TIMEOUT, DEFAULT_DIFFBOT_CONNECT_TIMEOUT));

        // must set default host
        options.setDefaultHost(config.getString(PROP_DIFFBOT_API_ENDPOINT, DEFAULT_DIFFBOT_API_ENDPOINT));
        // vertx stuff
        options.setKeepAlive(true);
        options.setMaxPoolSize(100);
        options.setVerifyHost(false);
        options.setTrustAll(true);
        if (config.getBoolean(PROP_DIFFBOT_SSL, DEFAULT_DIFFBOT_SSL)) {
            options.setSsl(true);
            options.setDefaultPort(443);
        }
        // Share vertx when possible. do not do this:
        // this.client = Vertx.vertx().createHttpClient(options);
        this.client = this.vertx.createHttpClient(options);
        log.info("Diffbot client=" + this.client);
        healthStatus = HealthStatusType.STATUS_NOTREACHED_YET;
    }
}

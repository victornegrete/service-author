package com.trendkite.service.extraction;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class DiffbotConstants {
    public static final String PROP_DIFFBOT_API_KEY = "api.key";
    public static final String DEFAULT_DIFFBOT_API_KEY = "d613c038432a4f8150f370507a9a8762" ;

    public static final String PROP_DIFFBOT_API_ENDPOINT = "api.endpoint";
    public static final String DEFAULT_DIFFBOT_API_ENDPOINT = "api.diffbot.com" ;
    public static final String DIFFBOT_API = "/article" ;

    public static final String PROP_DIFFBOT_SOCKET_TIMEOUT = "socket.timeout";
    public static final int DEFAULT_DIFFBOT_SOCKET_TIMEOUT = 10000;

    public static final String PROP_DIFFBOT_CONNECT_TIMEOUT = "connect.timeout";
    public static final int DEFAULT_DIFFBOT_CONNECT_TIMEOUT = 10000;

    public static final String PROP_DIFFBOT_ARTICLE_TIMEOUT = "article.timeout";
    public static final int DEFAULT_DIFFBOT_ARTICLE_TIMEOUT = 50000;

    public static final String PROP_DIFFBOT_API_VERSION = "api.version";
    public static final String DEFAULT_DIFFBOT_API_VERSION = "v3" ;

    public static final String PROP_DIFFBOT_ENABLED = "enabled";
    public static final boolean DEFAULT_DIFFBOT_ENABLED = true ;

    public static final String PROP_DIFFBOT_SSL = "ssl";
    public static final boolean DEFAULT_DIFFBOT_SSL = false ;

    public static final String PROP_DIFFBOT_RETRIES = "retries";
    public static final int DEFAULT_DIFFBOT_RETRIES = 3 ;

    public static final String QPARAM_DIFFBOT_API_KEY="token";
    public static final String QPARAM_DIFFBOT_ARTICLE_URL="url";
    public static final String QPARAM_DIFFBOT_ARTICLE_TIMEOUT="timeout";

    public static final String MEDIATYPE_APPLICATION_JSON = "application/json";
    public static final String HEADER_DIFFBOT_REFERER="X-Forward-Referer";
    public static final String DEFAULT_REFERER_HEADER="https://www.google.com/";

    public static final String METRIC_DIFFBOT_API_CALLS="diffbot-apicalls";
    public static final String METRIC_DIFFBOT_DATA_SUCCESS="diffbot-success"; // includes calls that return errors (like 404)
    public static final String METRIC_DIFFBOT_RESPONSE_FAILURE="diffbot-failure";
    public static final String METRIC_DIFFBOT_RESPONSE_EXCEPTION="diffbot-exception";
    public static final String METRIC_DIFFBOT_UNENCODEABLE_URLS="diffbot-unencodeable-urls";
    public static final String METRIC_DIFFBOT_UNEXPECTED_RESPONSE="diffbot-unexpected-json-response";

    public String computeEndpoint(String apiVersion) {
        return "/" + apiVersion + DIFFBOT_API;
    }

    public String computeUri(String endPoint, String apiKey, String url, int timeout) throws UnsupportedEncodingException {
        // this is pretty dumb... isn't there a better way to pass query args?
        return endPoint+"?"
                + QPARAM_DIFFBOT_API_KEY+"="+apiKey+"&"
                + QPARAM_DIFFBOT_ARTICLE_TIMEOUT+"="+timeout+"&"
                + QPARAM_DIFFBOT_ARTICLE_URL+"="+URLEncoder.encode(url, "UTF-8");
    }
}
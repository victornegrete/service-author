package com.trendkite.service;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.DeploymentOptionsConverter;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.trendkite.api.ArticleConstants.SERVICE_APPLICATION_LABEL;

public class ArticleDeployer extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(ArticleDeployer.class);
    private static final int DEFAULT_WORKERPOOLSIZE = 3;
    @Override
    public void start() throws Exception {
        log.info("Article deployer start() vertx="+vertx);
        // config() returns the content of the file named in the -conf argument
        JsonObject cfg = config();
        if (cfg == null) {
            throw new IllegalArgumentException(SERVICE_APPLICATION_LABEL+": A configuration must be defined!");
        }
        log.info("Article deployer config argument="+cfg.toString());
        JsonObject deploymentConfig = (cfg.containsKey("deployment")) ? cfg.getJsonObject("deployment") : new JsonObject();
        log.info("Article deployer deployment config="+deploymentConfig.toString());
        JsonObject serviceConfig = (deploymentConfig.containsKey("service")) ? deploymentConfig.getJsonObject("service") : new JsonObject();
        log.info("Article deployer service config="+serviceConfig.toString());

        DeploymentOptions deploymentOpts = new DeploymentOptions();
        // defaults
        deploymentOpts.setWorkerPoolSize(DEFAULT_WORKERPOOLSIZE);
        deploymentOpts.setWorker(true);

        // override default options with the configuration
        DeploymentOptionsConverter.fromJson(deploymentConfig, deploymentOpts);
        // the reason the service config is not picked up automatically is that
        // it is under the 'service' property rather than the 'config' property
        // (current convention)
        deploymentOpts.setConfig(serviceConfig);
        log.info("ArticleService will be deployed with these options=" + deploymentOpts.toJson());

        vertx.deployVerticle(ArticleService.class.getName(),
                deploymentOpts,
                null
        );
    }
}


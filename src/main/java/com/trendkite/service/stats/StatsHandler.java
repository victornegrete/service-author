package com.trendkite.service.stats;

import com.trendkite.stats.datadog.DataDogStatsCollector;
import com.trendkite.stats.datadog.DataDogStatsCollectorFactory;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Properties;

public class StatsHandler {
    private static final String DATADOG_STATSD_HOST = "datadog_statsd_host";
    private static final String DATADOG_PORT = "datadog_port";
    private static final String DATADOG_TAGS = "datadog_tags";
    private static final int DEFAULT_DD_PORT = 8125;
    private static final String DEFAULT_DD_HOST = "localhost";
    private static final String SERVICE_TAG = "service-article";
    private static DataDogStatsCollector statsCollector;

    public static final String DD_NUM_REQUESTS = SERVICE_TAG+".requests_received";
    public static final String DD_NUM_OK_REQUESTS = SERVICE_TAG+".requests_success";
    public static final String DD_NUM_FAILED_REQUESTS = SERVICE_TAG+".requests_failure";
    public static final String DD_NUM_ARTICLES = SERVICE_TAG+".articles_extracted";
    public static final String DD_NUM_FAILED_ARTICLES = SERVICE_TAG+".articles_failed";


    public static DataDogStatsCollector getCollector(JsonObject config, List<String> tags) {
        if (statsCollector == null) {
            Properties datadogProps = new Properties();
            datadogProps.put(DATADOG_STATSD_HOST, config.getString(DATADOG_STATSD_HOST, DEFAULT_DD_HOST));
            datadogProps.put(DATADOG_PORT, ""+config.getInteger(DATADOG_PORT, DEFAULT_DD_PORT));
            datadogProps.put(DATADOG_TAGS, config.getString(DATADOG_TAGS, SERVICE_TAG));

            statsCollector = DataDogStatsCollectorFactory.getCollector(datadogProps, tags);
        }
        return statsCollector;
    }
}

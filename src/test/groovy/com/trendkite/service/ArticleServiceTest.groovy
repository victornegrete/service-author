package com.trendkite.service

import io.vertx.core.buffer.impl.BufferImpl
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.core.buffer.Buffer

import spock.lang.Specification

import com.trendkite.api.model.ArticleRequestUrl

class ArticleServiceTest extends Specification {

    def 'malformed incoming request: an empty json' () {
        setup:
            ArticleService svc = new ArticleService()
        when:
            JsonObject urlList = new JsonObject()
            List<ArticleRequestUrl> urls = svc.getUrls(urlList)
        then:
            urls.size() == 0
    }

    def 'malformed incoming request: null object' () {
        setup:
        ArticleService svc = new ArticleService()
        when:
        List<ArticleRequestUrl> urls = svc.getUrls(null)
        then:
        urls.size() == 0
    }

    def 'wrong incoming request: list with wrong key' () {
        setup:
        ArticleService svc = new ArticleService()
        when:
        JsonObject urlList = new JsonObject()
        urlList.put('urlwrongkey', new JsonArray())
        List<ArticleRequestUrl> urls = svc.getUrls(urlList)
        then:
        urls.size() == 0
    }

    def 'wrong incoming request: non-empty list of invalid object' () {
        setup:
        ArticleService svc = new ArticleService()
        when:
        JsonObject urlList = new JsonObject()
        JsonArray entries = new JsonArray()
        entries.add(new JsonObject())
        urlList.put('urls', entries)
        List<ArticleRequestUrl> urls = svc.getUrls(urlList)
        then:
        urls.size() == 0
    }

    def 'partically incoming request: non-empty list of valid and invalid object' () {
        setup:
        ArticleService svc = new ArticleService()
        when:
        JsonObject urlList = new JsonObject()
        JsonArray entries = new JsonArray()
        JsonObject e1 = new JsonObject()
        e1.put('url', 'http:uno.com')
        JsonObject e2 = new JsonObject()
        e2.put('urlwrong', 'http:dos.com')
        entries.add(e1) // good
        entries.add(new JsonObject()) // bad
        entries.add(e2) // bad
        urlList.put('urls', entries)
        List<ArticleRequestUrl> urls = svc.getUrls(urlList)
        then:
        urls.size() == 1
    }

    def 'correct request: empty list' () {
        setup:
        ArticleService svc = new ArticleService()
        when:
        JsonObject urlList = new JsonObject()
        JsonArray entries = new JsonArray()
        urlList.put('urls', entries)
        List<ArticleRequestUrl> urls = svc.getUrls(urlList)
        then:
        urls.size() == 0
    }
    def 'correct request: valid items in list' () {
        setup:
        ArticleService svc = new ArticleService()
        when:
        JsonObject urlList = new JsonObject()
        JsonArray entries = new JsonArray()
        JsonObject e1 = new JsonObject()
        e1.put('url', 'http:uno.com') // good
        JsonObject e2 = new JsonObject()
        e2.put('url', 'http:dos.com') // good
        entries.add(e1)
        entries.add(e2)
        urlList.put('urls', entries)
        List<ArticleRequestUrl> urls = svc.getUrls(urlList)
        then:
        urls.size() == 2
    }

}
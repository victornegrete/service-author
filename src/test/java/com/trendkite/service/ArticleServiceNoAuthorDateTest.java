package com.trendkite.service;

import com.trendkite.api.ArticleConstants;
import com.trendkite.api.client.ArticleClient;
import com.trendkite.api.model.ArticleExtracted;
import com.trendkite.api.model.ArticleRequest;
import com.trendkite.api.model.ArticleRequestUrl;
import com.trendkite.api.model.ArticleResponse;
import com.trendkite.service.extraction.DiffbotClient;
import com.trendkite.service.extraction.DiffbotConstants;
import com.trendkite.stats.StatsCollector;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

@RunWith(VertxUnitRunner.class)
public class ArticleServiceNoAuthorDateTest {
    private static final  org.slf4j.Logger logger = LoggerFactory.getLogger(ArticleServiceVertxTest.class);
    private Vertx vertx;
    private static int port=9066;
    private String host;
    private static final  StatsCollector statsCollector =  mock(StatsCollector.class);

    @Before
    public void setUp(TestContext context) throws IOException {
        // initializes new vertx in a free local port
        vertx = Vertx.vertx();
        //permissionerrorServerSocket socket = new ServerSocket(0);
        //port = socket.getLocalPort();
        //socket.close();
        port++;
        host="localhost";
        // Build configuration
        // read from test-config.json and override whatever else we need
        String testConfigFilename = "/test-configNoAuthorDate.json";
        URL fp = this.getClass().getResource(testConfigFilename);
        InputStream jsonStream = fp.openStream();
        String jsonStr = IOUtils.toString(jsonStream, "UTF-8");
        JsonObject config = new JsonObject(jsonStr);
        JsonObject deployment = config.getJsonObject("deployment");
        JsonObject service = deployment.getJsonObject("service");
        service.put("http.port", port);
        // configure and run
        DeploymentOptions options = new DeploymentOptions(config)
                .setConfig(service)
                ;
        vertx.deployVerticle(ArticleService.class.getName(), options, context.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void testManyUrlsRoundTripUsingSyncClient(TestContext context) {
        List <ArticleRequestUrl> urls = new ArrayList<>();
        String sUrl1 = "http://vox.com";
        String sUrl2="http://news.bbc.co.uk";
        String sUrl3 = "http://news.google.com";
        ArticleRequestUrl url1 = new ArticleRequestUrl(sUrl1);
        ArticleRequestUrl url2 = new ArticleRequestUrl(sUrl2);
        ArticleRequestUrl url3 = new ArticleRequestUrl(sUrl3);
        urls.add(url1);
        urls.add(url2);
        urls.add(url3);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new ArticleClient(config, statsCollector);
        ArticleResponse r = client.extractArticlesSync(request);
        context.assertTrue(!r.isError());
        context.assertTrue(r.getResults().size() == 3);
        context.assertTrue(r.get(sUrl1).getPageUrl().equals(sUrl1));
        context.assertTrue(r.get(sUrl2).getPageUrl().equals(sUrl2));
        context.assertTrue(r.get(sUrl3).getPageUrl().equals(sUrl3));
        context.assertTrue(StringUtils.isEmpty(r.getError()));
    }
}
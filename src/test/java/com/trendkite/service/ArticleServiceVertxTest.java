package com.trendkite.service;

import com.trendkite.api.ArticleConstants;
import com.trendkite.api.client.ArticleClient;
import com.trendkite.api.model.ArticleExtracted;
import com.trendkite.api.model.ArticleRequest;
import com.trendkite.api.model.ArticleRequestUrl;
import com.trendkite.api.model.ArticleResponse;
import com.trendkite.service.extraction.DiffbotClient;
import com.trendkite.service.extraction.DiffbotConstants;
import com.trendkite.stats.StatsCollector;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

@RunWith(VertxUnitRunner.class)
public class ArticleServiceVertxTest {
    private static final  org.slf4j.Logger logger = LoggerFactory.getLogger(ArticleServiceVertxTest.class);
    private Vertx vertx;
    private static int port=8866;
    private String host;
    private static final  StatsCollector statsCollector =  mock(StatsCollector.class);

    class TestArticleClient extends ArticleClient {
        private HttpClient localClient; // not static

        TestArticleClient (JsonObject config) {
            super(config, statsCollector);
        }
        @Override
        public void setClient(HttpClient c) {
            this.localClient = c;
        }
        @Override
        public HttpClient getClient() {
            return this.localClient;
        }
    }

    @Before
    public void setUp(TestContext context) throws IOException {
        // initializes new vertx in a free local port
        vertx = Vertx.vertx();
        //permissionerrorServerSocket socket = new ServerSocket(0);
        //port = socket.getLocalPort();
        //socket.close();
        port++;
        host="localhost";
        // Build configuration
        // read from test-config.json and override whatever else we need
        String testConfigFilename = "/test-config.json";
        URL fp = this.getClass().getResource(testConfigFilename);
        InputStream jsonStream = fp.openStream();
        String jsonStr = IOUtils.toString(jsonStream, "UTF-8");
        JsonObject config = new JsonObject(jsonStr);
        JsonObject deployment = config.getJsonObject("deployment");
        JsonObject service = deployment.getJsonObject("service");
        service.put("http.port", port);
        // configure and run
        DeploymentOptions options = new DeploymentOptions(config)
                .setConfig(service)
                ;
        vertx.deployVerticle(ArticleService.class.getName(), options, context.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void testBadRoundTrip(TestContext context) {
        final Async async = context.async();
        final String [] gotBody = {""};
        vertx.createHttpClient().getNow(port, host, ArticleConstants.SERVICE_URI,
                response -> {
                    response.handler(body -> {
                        logger.info(body.toString());
                        gotBody[0] = body.toString();
                        async.complete();
                    });
                });
        async.await();
        context.assertEquals(gotBody[0], "<html><body><h1>Resource not found</h1></body></html>");
    }

    @Test
    public void testErrorRoundTrip(TestContext context) {
        final Async async = context.async();
        ArticleRequest request = new ArticleRequest();
        String reqJsonString = Json.encodePrettily(request);
        String length = Integer.toString(reqJsonString.length());
        final int [] gotStatus = {-1};
        final MultiMap [] gotHeaders = {null};
        final ArticleResponse[] gotResponseJson = {null};
        vertx.createHttpClient().post(port, host, ArticleConstants.SERVICE_URI)
                .putHeader(HttpHeaders.CONTENT_TYPE, ArticleConstants.RESPONSE_CONTENT_TYPE_JSON)
                .putHeader(HttpHeaders.CONTENT_LENGTH, length)
                .handler( response -> {
                    gotStatus[0] = response.statusCode();
                    gotHeaders[0] = response.headers();
                    response.handler(body -> {
                        String sResponse = body.toString();
                        logger.info(sResponse);
                        gotResponseJson[0] = Json.decodeValue(sResponse, ArticleResponse.class);
                        async.complete();
                    });
                })
                .write(reqJsonString)
                .end();
        async.await();
        context.assertEquals(gotStatus[0], 404);
        //FAILS-don'tknowwhyyet//context.assertEquals(response.headers().get(ArticleConstants.HEADER_CONTENT_TYPE), ArticleConstants.RESPONSE_CONTENT_TYPE_JSON);
        context.assertTrue(gotHeaders[0].get(HttpHeaders.CONTENT_TYPE).contains(ArticleConstants.RESPONSE_CONTENT_TYPE_JSON));
        context.assertTrue(gotResponseJson[0].getError().contains(ArticleConstants.ERROR_URLS_NOT_FOUND));
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testUrlRoundTrip(TestContext context) {
        final Async async = context.async();
        List <ArticleRequestUrl> urls = new ArrayList<>();
        //String sUrl = "http://www.trendkite.com";
        String sUrl = "http://news.google.com";
        ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
        urls.add(url);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        String reqJsonString = Json.encodePrettily(request);
        String length = Integer.toString(reqJsonString.length());
        final int [] gotStatus = {-1};
        final MultiMap [] gotHeaders = {null};
        final ArticleResponse[] gotResponseJson = {null};
        vertx.createHttpClient().post(port, host, ArticleConstants.SERVICE_URI)
                .putHeader(HttpHeaders.CONTENT_TYPE, ArticleConstants.RESPONSE_CONTENT_TYPE_JSON)
                .putHeader(HttpHeaders.CONTENT_LENGTH, length)
                .handler( response -> {
                    gotStatus[0] = response.statusCode();
                    gotHeaders[0] = response.headers();
                    response.bodyHandler(body -> {
                        String sResponse = body.toString();
                        logger.info("Response body"+sResponse);
                        gotResponseJson[0] = Json.decodeValue(sResponse, ArticleResponse.class);
                        logger.info("Response decoded"+gotResponseJson[0]);
                        async.complete();
                    });
                })
                .write(reqJsonString)
                .end();
        async.await();
        context.assertEquals(gotStatus[0], 200);
        context.assertTrue(gotResponseJson[0].getResults().size() == 1);
        ArticleResponse r = gotResponseJson[0];
        context.assertTrue(r.get(sUrl).getPageUrl().equals(sUrl));
        context.assertTrue(StringUtils.isEmpty(gotResponseJson[0].getError()));
        context.assertEquals(gotHeaders[0].get(HttpHeaders.CONTENT_TYPE), ArticleConstants.RESPONSE_CONTENT_TYPE_JSON);
        context.assertTrue(gotHeaders[0].get(HttpHeaders.CONTENT_TYPE).contains(ArticleConstants.RESPONSE_CONTENT_TYPE_JSON));

    }
    @Test
    public void testJunkRoundTrip(TestContext context) {
        final Async async = context.async();
        String reqJsonString = "some junk here";
        String length = Integer.toString(reqJsonString.length());
        final int [] gotStatus = {-1};
        final MultiMap [] gotHeaders = {null};
        final ArticleResponse[] gotResponseJson = {null};
        vertx.createHttpClient().post(port, host, ArticleConstants.SERVICE_URI)
                .putHeader(HttpHeaders.CONTENT_TYPE, ArticleConstants.RESPONSE_CONTENT_TYPE_JSON)
                .putHeader(HttpHeaders.CONTENT_LENGTH, length)
                .handler( response -> {
                    gotStatus[0] = response.statusCode();
                    gotHeaders[0] = response.headers();
                    response.handler(body -> {
                        String sResponse = body.toString();
                        logger.info(sResponse);
                        gotResponseJson[0] = Json.decodeValue(sResponse, ArticleResponse.class);
                        async.complete();
                    });
                })
                .write(reqJsonString)
                .end();
        async.await();
        context.assertEquals(gotStatus[0], 400);
        context.assertTrue(gotResponseJson[0].getError().contains(ArticleConstants.ERROR_INVALID_JSON));
        context.assertEquals(gotHeaders[0].get(HttpHeaders.CONTENT_TYPE), ArticleConstants.RESPONSE_CONTENT_TYPE_JSON);
        context.assertTrue(gotHeaders[0].get(HttpHeaders.CONTENT_TYPE).contains(ArticleConstants.RESPONSE_CONTENT_TYPE_JSON));

    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testDiffbotClient(TestContext context) {
        final Async async = context.async();
        final ArticleExtracted[] gotArticle = {null};
        final Throwable[] gotError = {null};
        JsonObject config = new JsonObject();
        DiffbotClient diffbotClient = new DiffbotClient(vertx, config, statsCollector);
        //String testUrl="http://www.salon.com/2016/09/15/its-already-happening-yes-trump-is-closing-in-on-the-polls-but-heres-why-thats-meaningless/";
        //String testUrl="http://www.trendkite.com";
        String testUrl = "http://news.google.com";
        Future<ArticleExtracted> future = diffbotClient.fetch(testUrl);
        future.setHandler(h -> {
            if (h.succeeded()) {
                ArticleExtracted article = h.result();
                logger.info("testDiffbotClient received article");
                gotArticle[0] = article;
                async.complete();
            } else {
                Throwable reason = h.cause();
                logger.info("testDiffbotClient received exception",  reason);
                gotError[0] = reason;
                async.complete();
            }
         });
        async.await();
        context.assertNotNull(gotArticle[0]);
        context.assertNull(gotError[0]);
        ArticleExtracted a = gotArticle[0];
        context.assertNotNull(a.getPageUrl());
        context.assertNotNull(a.getSiteName());
        context.assertNotNull(a.getHtml());
        context.assertNotNull(a.getText());

    }

    @Test
    public void testDiffbotClientRetries(TestContext context) {
        class TestDiffbotClient extends DiffbotClient {
            private int testcount = 0;

            TestDiffbotClient(Vertx vertx, JsonObject config, StatsCollector statsCollector) {
                super(vertx, config, statsCollector);
                this.testcount = 0;
            }
            public int countCalls() { return this.testcount; }

            @Override
            public Future<ArticleExtracted> fetchOnce(String url, int trialNumber) {
                // shortcircuit the trip to diffbot
                Future<ArticleExtracted> future = Future.future();
                ArticleExtracted a = new ArticleExtracted();
                a.setPageUrl(url);
                a.setUrl(url);
                future.complete(a);
                testcount ++;
                return future;
            }
        }
        final Async async = context.async();
        final ArticleExtracted[] gotArticle = {null};
        final Throwable[] gotError = {null};
        JsonObject config = new JsonObject();
        int numRetries = 7;
        config.put(DiffbotConstants.PROP_DIFFBOT_RETRIES, numRetries);
        TestDiffbotClient diffbotClient = new TestDiffbotClient(vertx, config, statsCollector);
        String testUrl = "http://bogus.url.goes.here.com";
        Future<ArticleExtracted> future = diffbotClient.fetch(testUrl);
        future.setHandler(h -> {
            if (h.succeeded()) {
                ArticleExtracted article = h.result();
                logger.info("testDiffbotClient received article");
                gotArticle[0] = article;
                async.complete();
            } else {
                Throwable reason = h.cause();
                logger.info("testDiffbotClient received exception",  reason);
                gotError[0] = reason;
                async.complete();
            }
        });
        async.await();
        context.assertNotNull(gotArticle[0]);
        context.assertEquals(diffbotClient.countCalls(), numRetries);
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testUrlRoundTripUsingClient(TestContext context) {
        final Async async = context.async();
        final ArticleResponse[] responses = {null};
        final String[] errorResponses = {null};
        List<ArticleRequestUrl> urls = new ArrayList<>();
        //String sUrl = "http://www.trendkite.com";
        String sUrl = "http://news.google.com";
        ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
        urls.add(url);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new TestArticleClient(config);
        context.assertTrue(client.health());
        Future<ArticleResponse> r = client.extractArticles(request);
        r.setHandler(h -> {
            if (h.succeeded()) {
                responses[0] = h.result();
            } else {
                h.cause().printStackTrace();
                errorResponses[0] = h.cause().getMessage();
            }
            async.complete();
        });
        async.await();
        ArticleResponse response = responses[0];
        String errorResponse = errorResponses[0];
        context.assertNotNull(response);
        context.assertNull(errorResponse);
        context.assertTrue(response.getResults().size() == 1);
        context.assertTrue(response.get(sUrl).getPageUrl().equals(sUrl));
        context.assertFalse(response.get(sUrl).isError());
        // broken unless we setup author and date now
        // context.assertTrue(client.health());
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testTwoUrls(TestContext context) {
        final Async async = context.async();
        final ArticleResponse[] responses = {null};
        final String[] errorResponses = {null};
        List <ArticleRequestUrl> urls = new ArrayList<>();
        //String sUrl = "http://www.trendkite.com";
        String sUrl = "http://news.google.com";
        String sUrl2 = "http://news.bbc.co.uk";
        ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
        ArticleRequestUrl url2 = new ArticleRequestUrl(sUrl2);
        urls.add(url);
        urls.add(url2);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new TestArticleClient(config);
        Future<ArticleResponse> r = client.extractArticles(request);
        r.setHandler( h -> {
            if (h.succeeded()) {
                responses[0] = h.result();
            } else {
                h.cause().printStackTrace();
                errorResponses[0] = h.cause().getMessage();
            }
            async.complete();
        });
        async.await();
        ArticleResponse response = responses[0];
        String errorResponse = errorResponses[0];
        context.assertNotNull(response);
        context.assertNull(errorResponse);
        context.assertTrue(response.getResults().size() == 2);
        context.assertTrue(response.get(sUrl).getPageUrl().equals(sUrl));
        context.assertTrue(response.get(sUrl2).getPageUrl().equals(sUrl2));
        context.assertFalse(response.get(sUrl).isError());
        context.assertFalse(response.get(sUrl2).isError());
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testTwoOneBadUrls(TestContext context) {
        final Async async = context.async();
        final ArticleResponse[] responses = {null};
        final String[] errorResponses = {null};
        List <ArticleRequestUrl> urls = new ArrayList<>();
        String sUrl = "somejunk";
        String sUrl2 = "http://news.bbc.co.uk";
        ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
        ArticleRequestUrl url2 = new ArticleRequestUrl(sUrl2);
        urls.add(url);
        urls.add(url2);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new TestArticleClient(config);
        context.assertTrue(client.health());
        Future<ArticleResponse> r = client.extractArticles(request);
        r.setHandler( h -> {
            if (h.succeeded()) {
                responses[0] = h.result();
            } else {
                h.cause().printStackTrace();
                errorResponses[0] = h.cause().getMessage();
            }
            async.complete();
        });
        async.await();
        ArticleResponse response = responses[0];
        String errorResponse = errorResponses[0];
        context.assertTrue(errorResponse == null);
        context.assertTrue(response.getResults().size() == 2);
        context.assertTrue(response.get(sUrl2).getPageUrl().equals(sUrl2));
        context.assertFalse(response.get(sUrl2).isError());
        context.assertTrue(response.get(sUrl).getUrl().equals(sUrl));
        context.assertTrue(response.get(sUrl).isError());
        // broken now that health also checks on author and date
        // context.assertTrue(client.health());
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testTwoBadUrls(TestContext context) {
        final Async async = context.async();
        final ArticleResponse[] responses = {null};
        final String[] errorResponses = {null};
        List <ArticleRequestUrl> urls = new ArrayList<>();
        String sUrl = "somejunk";
        String sUrl2 = "otherjunk";
        ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
        ArticleRequestUrl url2 = new ArticleRequestUrl(sUrl2);
        urls.add(url);
        urls.add(url2);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new TestArticleClient(config);
        Future<ArticleResponse> r = client.extractArticles(request);

        r.setHandler( h -> {
            if (h.succeeded()) {
                responses[0] = h.result();
            } else {
                h.cause().printStackTrace();
                errorResponses[0] = h.cause().getMessage();
            }
            async.complete();
        });
        async.await();
        ArticleResponse response = responses[0];
        String errorResponse = errorResponses[0];
        context.assertNotNull(response);
        context.assertNull(errorResponse);
        context.assertTrue(response.getResults().size() == 2);
        context.assertTrue(response.get(sUrl).isError());
        context.assertTrue(response.get(sUrl2).isError());
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testAuthor(TestContext context) {
        final Async async = context.async();
        final ArticleResponse[] responses = {null};
        final String[] errorResponses = {null};
        List <ArticleRequestUrl> urls = new ArrayList<>();
        //String sUrl = "http://www.kentucky.com/living/religion/paul-prather/article96645282.html";
        String sUrl = "http://www.salon.com/2016/09/20/why-trumps-murky-web-of-international-business-deals-is-a-problem-and-one-that-cant-be-fixed/";
        ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
        urls.add(url);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new TestArticleClient(config);
        Future<ArticleResponse> r = client.extractArticles(request);
        r.setHandler( h -> {
            if (h.succeeded()) {
                responses[0] = h.result();
            } else {
                h.cause().printStackTrace();
                errorResponses[0] = h.cause().getMessage();
            }
            async.complete();
        });
        async.await();
        ArticleResponse response = responses[0];
        String errorResponse = errorResponses[0];
        context.assertTrue(response.getResults().size() == 1);
        context.assertTrue(response.get(sUrl).getPageUrl().equals(sUrl));
        context.assertTrue(!response.get(sUrl).getAuthor().equals(""));
        context.assertFalse(response.get(sUrl).isError());
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testDate(TestContext context) {
        final Async async = context.async();
        final ArticleResponse[] responses = {null};
        final String[] errorResponses = {null};
        List <ArticleRequestUrl> urls = new ArrayList<>();
        String sUrl1 = "https://www.elastic.co/blog/welcome-prelert-to-the-elastic-team";
        String sUrl2 = "http://www.salon.com/2016/09/20/why-trumps-murky-web-of-international-business-deals-is-a-problem-and-one-that-cant-be-fixed/";
        ArticleRequestUrl url = new ArticleRequestUrl(sUrl1);
        ArticleRequestUrl url2 = new ArticleRequestUrl(sUrl2);
        urls.add(url);
        urls.add(url2);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new TestArticleClient(config);
        Future<ArticleResponse> r = client.extractArticles(request);
        r.setHandler( h -> {
            if (h.succeeded()) {
                responses[0] = h.result();
            } else {
                h.cause().printStackTrace();
                errorResponses[0] = h.cause().getMessage();
            }
            async.complete();
        });
        async.await();
        ArticleResponse response = responses[0];
        String errorResponse = errorResponses[0];
        context.assertNotNull(response);
        context.assertNull(errorResponse);
        context.assertTrue(response.getResults().size() == 2);
        context.assertTrue(response.get(sUrl1).getPageUrl().equals(sUrl1));
        context.assertTrue(!response.get(sUrl1).getAuthor().equals(""));
        context.assertTrue(!response.get(sUrl1).getDate().equals(""));
        context.assertFalse(response.get(sUrl1).isError());
        context.assertTrue(response.get(sUrl2).getPageUrl().equals(sUrl2));
        context.assertTrue(!response.get(sUrl2).getAuthor().equals(""));
        context.assertTrue(!response.get(sUrl2).getDate().equals(""));
        context.assertFalse(response.get(sUrl2).isError());
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testUrlRoundTripUsingSyncClient(TestContext context) {
        List <ArticleRequestUrl> urls = new ArrayList<>();
        //String sUrl = "http://www.trendkite.com";
        String sUrl = "http://news.google.com";
        ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
        urls.add(url);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new TestArticleClient(config);
        ArticleResponse r = client.extractArticlesSync(request);
        context.assertTrue(r.getResults().size() == 1);
        context.assertTrue(r.get(sUrl).getPageUrl().equals(sUrl));
        context.assertTrue(StringUtils.isEmpty(r.getError()));
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testManyUrlsRoundTripUsingSyncClient(TestContext context) {
        List <ArticleRequestUrl> urls = new ArrayList<>();
        String sUrl1 = "http://vox.com";
        String sUrl2="http://news.bbc.co.uk";
        String sUrl3 = "http://news.google.com";
        ArticleRequestUrl url1 = new ArticleRequestUrl(sUrl1);
        ArticleRequestUrl url2 = new ArticleRequestUrl(sUrl2);
        ArticleRequestUrl url3 = new ArticleRequestUrl(sUrl3);
        urls.add(url1);
        urls.add(url2);
        urls.add(url3);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new TestArticleClient(config);
        ArticleResponse r = client.extractArticlesSync(request);
        context.assertTrue(r.getResults().size() == 3);
        context.assertTrue(r.get(sUrl1).getPageUrl().equals(sUrl1));
        context.assertTrue(r.get(sUrl2).getPageUrl().equals(sUrl2));
        context.assertTrue(r.get(sUrl3).getPageUrl().equals(sUrl3));
        context.assertTrue(StringUtils.isEmpty(r.getError()));
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testSyncClientAgainstDevService(TestContext context) {
        List <ArticleRequestUrl> urls = new ArrayList<>();
        String sUrl1 = "http://vox.com";
        String sUrl2="http://news.bbc.co.uk";
        String sUrl3 = "http://news.google.com";
        ArticleRequestUrl url1 = new ArticleRequestUrl(sUrl1);
        ArticleRequestUrl url2 = new ArticleRequestUrl(sUrl2);
        ArticleRequestUrl url3 = new ArticleRequestUrl(sUrl3);
        urls.add(url1);
        urls.add(url2);
        urls.add(url3);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_ENV, "dev");
        // DNS not working from my laptop
        //config.put(ArticleConstants.CLIENT_OPTION_HOST, "10.0.11.136");
        ArticleClient client = new TestArticleClient(config);
        context.assertTrue(client.health());
        ArticleResponse r = client.extractArticlesSync(request);
        context.assertTrue(r.getResults().size() == 3);
        context.assertEquals(r.get(sUrl1).getPageUrl(), sUrl1);
        context.assertEquals(r.get(sUrl2).getPageUrl(), sUrl2);
        context.assertEquals(r.get(sUrl3).getPageUrl(), sUrl3);
        context.assertTrue(StringUtils.isEmpty(r.getError()));
        context.assertTrue(client.health());
    }

    @Ignore // talks to runnning services (ignore during build, use to debug)
    @Test
    public void testHealthCheck(TestContext context) {
        JsonObject goodConfig = new JsonObject();
        goodConfig.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        goodConfig.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient goodClient = new TestArticleClient(goodConfig);
        context.assertTrue(goodClient.health());

        List <ArticleRequestUrl> urls = new ArrayList<>();
        String sUrl1 = "http://trendkite.com";
        ArticleRequestUrl url1 = new ArticleRequestUrl(sUrl1);
        urls.add(url1);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        ArticleResponse r = goodClient.extractArticlesSync(request);
        context.assertTrue(r.getResults().size() == 1);
        context.assertFalse(r.get(sUrl1).isError());
        context.assertTrue(goodClient.health());
    }

    @Test
    public void testBadServer(TestContext context) {
        JsonObject badConfig = new JsonObject();
        // used this to test the case in which the load balancer did not have the instance
        //badConfig.put(ArticleConstants.CLIENT_OPTION_HOST, "article-dev-main.aws.trendkite.com");
        //badConfig.put(ArticleConstants.CLIENT_OPTION_PORT, 80);

        // used this to test the case of bad port for correct instance (get a timeout)
        //badConfig.put(ArticleConstants.CLIENT_OPTION_HOST, "10.0.11.136");
        //badConfig.put(ArticleConstants.CLIENT_OPTION_PORT, 6666);

        // used this to test the case of host name that can't be resolved
        badConfig.put(ArticleConstants.CLIENT_OPTION_HOST, "wronghost");
        badConfig.put(ArticleConstants.CLIENT_OPTION_PORT, 6666);
        ArticleClient badClient = new TestArticleClient(badConfig);
        String sUrl1 = "http://foolala.tralala.yole";
        ArticleRequestUrl url1 = new ArticleRequestUrl(sUrl1);
        List <ArticleRequestUrl> urls = new ArrayList<>();
        urls.add(url1);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        ArticleResponse r = badClient.extractArticlesSync(request);
        context.assertTrue(r != null);
        context.assertTrue(r.getError() != null);
    }

    @Ignore
    @Test
    public void testHealthResponse(TestContext context) {
        final Async async = context.async();
        JsonObject goodConfig = new JsonObject();
        goodConfig.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        goodConfig.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient goodClient = new TestArticleClient(goodConfig);
        final JsonArray[] gotResponseJson = {null};
        goodClient.healthResponse().setHandler( h -> {
            if (h.succeeded()) {
                gotResponseJson[0] = h.result();
            }
            async.complete();
        });
        async.await();
        context.assertTrue(gotResponseJson[0] != null);
        JsonArray response = gotResponseJson[0];
        context.assertEquals(response.size(), 3);
        JsonObject uno = response.getJsonObject(0);
        JsonObject dos = response.getJsonObject(1);
        JsonObject tres = response.getJsonObject(2);
        String name ;
        String value ;
        name = uno.getString("name");
        value = uno.getString("status");
        context.assertEquals(name, "subordinate-author-service");
        context.assertEquals(value, "OK");
        name = dos.getString("name");
        value = dos.getString("status");
        context.assertEquals(name, "subordinate-date-service");
        context.assertEquals(value, "OK");
        name = tres.getString("name");
        value = tres.getString("status");
        context.assertEquals(name, "subordinate-diffbot-service");
        context.assertEquals(value, "BAD");
    }
}
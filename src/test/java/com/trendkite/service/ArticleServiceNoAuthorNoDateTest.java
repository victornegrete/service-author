package com.trendkite.service;

import com.trendkite.api.ArticleConstants;
import com.trendkite.api.client.ArticleClient;
import com.trendkite.api.model.ArticleExtracted;
import com.trendkite.api.model.ArticleRequest;
import com.trendkite.api.model.ArticleRequestUrl;
import com.trendkite.api.model.ArticleResponse;
import com.trendkite.service.extraction.DiffbotClient;
import com.trendkite.stats.StatsCollector;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.mockito.Mockito.mock;

@RunWith(VertxUnitRunner.class)
public class ArticleServiceNoAuthorNoDateTest {
    private static final  org.slf4j.Logger logger = LoggerFactory.getLogger(ArticleServiceVertxTest.class);
    private Vertx vertx;
    private static int port=8966;
    private String host;
    private static final  StatsCollector statsCollector =  mock(StatsCollector.class);
    private JsonObject service;
    @Before
    public void setUp(TestContext context) throws IOException {
        // initializes new vertx in a free local port
        vertx = Vertx.vertx();
        //permissionerrorServerSocket socket = new ServerSocket(0);
        //port = socket.getLocalPort();
        //socket.close();
        port++;
        host="localhost";
        // Build configuration
        // read from test-config.json and override whatever else we need
        String testConfigFilename = "/test-configNoAuthorNoDate.json";
        URL fp = this.getClass().getResource(testConfigFilename);
        InputStream jsonStream = fp.openStream();
        String jsonStr = IOUtils.toString(jsonStream, "UTF-8");
        JsonObject config = new JsonObject(jsonStr);
        JsonObject deployment = config.getJsonObject("deployment");
        service = deployment.getJsonObject("service");
        service.put("http.port", port);
        // configure and run
        DeploymentOptions options = new DeploymentOptions(config)
                .setConfig(service)
                ;
        vertx.deployVerticle(ArticleService.class.getName(), options, context.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void testManyUrlsRoundTripUsingSyncClient(TestContext context) {
        List <ArticleRequestUrl> urls = new ArrayList<>();
        String sUrl1 = "http://vox.com";
        String sUrl2="http://news.bbc.co.uk";
        String sUrl3 = "http://news.google.com";
        ArticleRequestUrl url1 = new ArticleRequestUrl(sUrl1);
        ArticleRequestUrl url2 = new ArticleRequestUrl(sUrl2);
        ArticleRequestUrl url3 = new ArticleRequestUrl(sUrl3);
        urls.add(url1);
        urls.add(url2);
        urls.add(url3);
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        config.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient client = new ArticleClient(config, statsCollector);
        ArticleResponse r = client.extractArticlesSync(request);
        context.assertTrue(!r.isError());
        context.assertTrue(r.getResults().size() == 3);
        context.assertTrue(r.get(sUrl1).getPageUrl().equals(sUrl1));
        context.assertTrue(r.get(sUrl2).getPageUrl().equals(sUrl2));
        context.assertTrue(r.get(sUrl3).getPageUrl().equals(sUrl3));
        context.assertTrue(StringUtils.isEmpty(r.getError()));
    }

    @Ignore
    @Test
    public void comparePerf(TestContext context) {
        // Create clients
        JsonObject config = new JsonObject();
        config.put(ArticleConstants.CLIENT_OPTION_HOST, "article-dev-main.aws.trendkite.com");
        config.put(ArticleConstants.CLIENT_OPTION_PORT, 80);
        ArticleClient client = new ArticleClient(config, statsCollector);

        JsonObject localConfig = new JsonObject();
        localConfig.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        localConfig.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient localClient = new ArticleClient(localConfig, statsCollector);

        JsonObject diffbotConfig = service.getJsonObject("diffbot");
        DiffbotClient diffbot = new DiffbotClient(vertx, diffbotConfig, statsCollector);

        // Test data
        List<String> testUrls = new ArrayList<String>();
        //testUrls.add("http://vox.com");
        testUrls.add("http://news.bbc.co.uk");
        //testUrls.add("http://news.google.com");
        final Async async = context.async();
        final AtomicInteger waitFor = new AtomicInteger(3 * testUrls.size());
        logger.info("WAIT (start) COUNT"+waitFor.get());
        for (String sUrl: testUrls) {
            // create request object
            ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
            List <ArticleRequestUrl> urls = new ArrayList<>();
            urls.add(url);
            ArticleRequest request = new ArticleRequest();
            request.setUrls(urls);

            // remote article service sync
            long articleStartTime = System.currentTimeMillis();
            ArticleResponse r = client.extractArticlesSync(request);
            long articleResponseTime = System.currentTimeMillis() - articleStartTime;
            logger.info("["+sUrl+"]ArticleSync="+articleResponseTime+"ms");

            // diffbot
            long diffbotStartTime = System.currentTimeMillis();
            Future<ArticleExtracted> diffbotArticleFuture = diffbot.fetch(sUrl);
            diffbotArticleFuture.setHandler(h -> {
                long diffbotResponseTime = System.currentTimeMillis() - diffbotStartTime;
                logger.info("["+sUrl+"]Diffbot="+diffbotResponseTime+"ms");
                if (waitFor.decrementAndGet() == 0) {
                    async.complete();
                }
                logger.info("WAIT (diffbot) COUNT"+waitFor.get());
            });

            // remote article service async
            long asyncArticleStartTime = System.currentTimeMillis();
            Future<ArticleResponse> rFuture = client.extractArticles(request);
            rFuture.setHandler(h -> {
                long asyncArticleResponseTime = System.currentTimeMillis() - asyncArticleStartTime;
                logger.info("["+sUrl+"]ArticleASync="+asyncArticleResponseTime+"ms");
                if (waitFor.decrementAndGet() == 0) {
                    async.complete();
                }
                logger.info("WAIT (dev article)  COUNT"+waitFor.get());
            });

            // local service - sync
            long localArticleStartTime = System.currentTimeMillis();
            ArticleResponse localR = localClient.extractArticlesSync(request);
            long localArticleResponseTime = System.currentTimeMillis() - localArticleStartTime;
            logger.info("["+sUrl+"]LocalArticleSync="+localArticleResponseTime+"ms");

            // local service - async
            long localAsyncArticleStartTime = System.currentTimeMillis();
            Future<ArticleResponse> localrFuture = localClient.extractArticles(request);
            localrFuture.setHandler(h -> {
                long localAsyncArticleResponseTime = System.currentTimeMillis() - localAsyncArticleStartTime;
                logger.info("["+sUrl+"]LocalArticleASync="+localAsyncArticleResponseTime+"ms");
                if (waitFor.decrementAndGet() == 0) {
                    async.complete();
                }
                logger.info("WAIT (local article) COUNT"+waitFor.get());
            });
        }
        async.await();
    }

    @Ignore
    @Test
    public void debugPerf (TestContext context) {
        // Create clients
        JsonObject localConfig = new JsonObject();
        localConfig.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        localConfig.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient localClient = new ArticleClient(localConfig, statsCollector);

        JsonObject diffbotConfig = service.getJsonObject("diffbot");
        DiffbotClient diffbot = new DiffbotClient(vertx, diffbotConfig, statsCollector);

        // Test data
        List<String> testUrls = new ArrayList<String>();
        testUrls.add("http://vox.com");
        testUrls.add("http://news.bbc.co.uk");
        testUrls.add("http://news.google.com");
        final Async async = context.async();
        final AtomicInteger waitFor = new AtomicInteger(2 * testUrls.size());
        logger.info("WAIT (start) COUNT"+waitFor.get());
        for (String sUrl: testUrls) {
            // create request object
            ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
            List <ArticleRequestUrl> urls = new ArrayList<>();
            urls.add(url);
            ArticleRequest request = new ArticleRequest();
            request.setUrls(urls);

            // diffbot
            long diffbotStartTime = System.currentTimeMillis();
            Future<ArticleExtracted> diffbotArticleFuture = diffbot.fetch(sUrl);
            diffbotArticleFuture.setHandler(h -> {
                long diffbotResponseTime = System.currentTimeMillis() - diffbotStartTime;
                logger.info("["+sUrl+"]Diffbot="+diffbotResponseTime+"ms");
                if (waitFor.decrementAndGet() == 0) {
                    async.complete();
                }
                logger.info("WAIT (diffbot) COUNT"+waitFor.get());
            });

            // local service - sync
            long localArticleStartTime = System.currentTimeMillis();
            ArticleResponse localR = localClient.extractArticlesSync(request);
            long localArticleResponseTime = System.currentTimeMillis() - localArticleStartTime;
            logger.info("["+sUrl+"]LocalArticleSync="+localArticleResponseTime+"ms");

            // local service - async
            long localAsyncArticleStartTime = System.currentTimeMillis();
            Future<ArticleResponse> localrFuture = localClient.extractArticles(request);
            localrFuture.setHandler(h -> {
                long localAsyncArticleResponseTime = System.currentTimeMillis() - localAsyncArticleStartTime;
                logger.info("["+sUrl+"]LocalArticleASync="+localAsyncArticleResponseTime+"ms");
                if (waitFor.decrementAndGet() == 0) {
                    async.complete();
                }
                logger.info("WAIT (local article) COUNT"+waitFor.get());
            });
        }
        async.await();
    }

    @Test
    public void testJsonEncoding (TestContext context) {
        // Create clients
        JsonObject localConfig = new JsonObject();
        localConfig.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        localConfig.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient localClient = new ArticleClient(localConfig, statsCollector);

        // Test data
        List<String> testUrls = new ArrayList<String>();
        String url1="http://vox.com";
        String url2="http://news.bbc.co.uk";
        String url3="http://news.google.com";
        testUrls.add(url1);
        testUrls.add(url2);
        testUrls.add(url3);

        List <ArticleRequestUrl> urls = new ArrayList<>();
        for (String sUrl: testUrls) {
            // create request object
            ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
            urls.add(url);
        }
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);

        ArticleResponse response = localClient.extractArticlesSync(request);
        JsonObject responseJson = response.convertToJson();
        context.assertEquals(response.getResults().size(), urls.size());
        for (String sUrl: testUrls) {
            ArticleExtracted a = response.getResults().get(sUrl);
            context.assertTrue(a.getUrl() != null);
        }
        // case 1: as is
        context.assertTrue(responseJson.containsKey("error"));
        context.assertTrue(responseJson.containsKey("results"));
        JsonObject articles = responseJson.getJsonObject("results");
        context.assertTrue(articles.containsKey(url1));
        context.assertTrue(articles.containsKey(url2));
        context.assertTrue(articles.containsKey(url3));

        // case 2: with synonyms, do not keep all fields
        Map<String, String> synonyms = new HashMap<String, String>();
        synonyms.put("original_url", "url");
        synonyms.put("url", "pageUrl");
        synonyms.put("new_text", "text");
        JsonObject responseJson2 = response.convertToJson(synonyms, "new_error", false);
        context.assertTrue(responseJson2.containsKey("new_error"));
        context.assertTrue(responseJson2.containsKey("results"));
        context.assertFalse(responseJson2.containsKey("error"));
        JsonObject articles2 = responseJson2.getJsonObject("results");
        context.assertTrue(articles2.containsKey(url1));
        context.assertTrue(articles2.containsKey(url2));
        context.assertTrue(articles2.containsKey(url3));
        context.assertTrue(articles2.getJsonObject(url1).containsKey("original_url"));
        context.assertTrue(articles2.getJsonObject(url1).containsKey("url"));
        context.assertTrue(articles2.getJsonObject(url1).containsKey("new_text"));
        context.assertFalse(articles2.getJsonObject(url1).containsKey("text"));

        // case3: with synonyms, keep all fields
        JsonObject responseJson3 = response.convertToJson(synonyms, "new_error");
        context.assertTrue(responseJson3.containsKey("new_error"));
        context.assertTrue(responseJson3.containsKey("results"));
        context.assertTrue(responseJson3.containsKey("error"));
        JsonObject articles3 = responseJson3.getJsonObject("results");
        context.assertTrue(articles3.containsKey(url1));
        context.assertTrue(articles3.containsKey(url2));
        context.assertTrue(articles3.containsKey(url3));
        context.assertTrue(articles3.getJsonObject(url1).containsKey("original_url"));
        context.assertTrue(articles3.getJsonObject(url1).containsKey("url"));
        context.assertTrue(articles3.getJsonObject(url1).containsKey("new_text"));
        context.assertTrue(articles3.getJsonObject(url1).containsKey("text"));
    }

    @Ignore
    @Test
    public void testBlacklisting (TestContext context) {
        // Create clients
        JsonObject localConfig = new JsonObject();
        localConfig.put(ArticleConstants.CLIENT_OPTION_HOST, host);
        localConfig.put(ArticleConstants.CLIENT_OPTION_PORT, port);
        ArticleClient localClient = new ArticleClient(localConfig, statsCollector);

        // Test data
        List<String> testUrls = new ArrayList<String>();
        String url1="http://Imblacklisted.pdf";
        String url2="http://news.bbc.co.uk";
        String url3="http://news.google.com";
        testUrls.add(url1);
        testUrls.add(url2);
        testUrls.add(url3);

        List <ArticleRequestUrl> urls = new ArrayList<>();
        for (String sUrl: testUrls) {
            // create request object
            ArticleRequestUrl url = new ArticleRequestUrl(sUrl);
            urls.add(url);
        }
        ArticleRequest request = new ArticleRequest();
        request.setUrls(urls);

        ArticleResponse response = localClient.extractArticlesSync(request);
        context.assertFalse(response.isError());
        context.assertTrue(response.getResults().values().size() == testUrls.size());
        context.assertTrue(response.getResults().get(url1).isError());
        context.assertTrue(response.getResults().get(url1).convertToHttpError().code() == 501);
        context.assertFalse(response.getResults().get(url2).isError());
        context.assertFalse(response.getResults().get(url3).isError());
    }
}

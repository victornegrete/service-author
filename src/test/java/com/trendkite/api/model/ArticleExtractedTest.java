package com.trendkite.api.model;

import io.netty.handler.codec.http.HttpResponseStatus;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArticleExtractedTest {

    @Test
    public void testBlankVsEmpty() throws Exception {
        assertTrue(StringUtils.isEmpty(null) && StringUtils.isBlank(null));
        assertTrue(StringUtils.isEmpty("") && StringUtils.isBlank(""));
        assertFalse(StringUtils.isEmpty(" "));
        assertTrue(StringUtils.isBlank(" "));
    }

    @Test public void testHttpCode() throws Exception {
        ArticleExtracted a = new ArticleExtracted();
        HttpResponseStatus r = a.convertToHttpError();
        assertNull(r);

        a.setErrorMessage("foo");
        r = a.convertToHttpError();
        assertNull(r);

        a.setErrorCode("55"); // invalid as http code
        r = a.convertToHttpError();
        assertNotNull(r);
        assertEquals(r.code(), 55);

        a.setErrorCode("404");
        r = a.convertToHttpError();
        assertNotNull(r);
        assertEquals(r.code(), 404);
    }
}

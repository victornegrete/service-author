package com.trendkite.api;

import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class ArticleConstantsTest {

    @Test public void testDefaultHost() throws Exception {
        ArticleConstants ac = new ArticleConstants();
        JsonObject config = new JsonObject();
        String host;

        host=ac.getHostForClient(config);
        assertEquals(host, ArticleConstants.SERVICE_DEFAULT_HOST);

        config.put(ArticleConstants.CLIENT_OPTION_ENV, "dev");
        host=ac.getHostForClient(config);
        assertEquals(host, "article-dev-main.aws.trendkite.com");

        config.put(ArticleConstants.CLIENT_OPTION_ENV, "prod");
        host=ac.getHostForClient(config);
        assertEquals(host, "article-prod-main.aws.trendkite.com");

        config.put(ArticleConstants.CLIENT_OPTION_ENV, "local");
        host=ac.getHostForClient(config);
        assertEquals(host,ArticleConstants.SERVICE_DEFAULT_HOST );

        String fakeHost = "foolala";
        config.put(ArticleConstants.CLIENT_OPTION_HOST, fakeHost);
        String casenames[] = new String[]{"dev", "prod", "local", "foo", fakeHost};
        List<String> cases = Arrays.asList(casenames);
        for (String env: cases) {
            config.put(ArticleConstants.CLIENT_OPTION_ENV, env);
            host=ac.getHostForClient(config);
            assertEquals(host, fakeHost );
        }
    }

    @Test public void testDefaultPort() throws Exception {
        ArticleConstants ac = new ArticleConstants();
        JsonObject config = new JsonObject();
        int port;

        port=ac.getPortForClient(config);
        assertEquals(port, ArticleConstants.SERVICE_DEFAULT_PORT);

        config.put(ArticleConstants.CLIENT_OPTION_ENV, "dev");
        port=ac.getPortForClient(config);
        assertEquals(port, ArticleConstants.SERVICE_DEFAULT_PORT);

        config.put(ArticleConstants.CLIENT_OPTION_ENV, "prod");
        port=ac.getPortForClient(config);
        assertEquals(port, ArticleConstants.SERVICE_DEFAULT_PORT);

        config.put(ArticleConstants.CLIENT_OPTION_ENV, "local");
        port=ac.getPortForClient(config);
        assertEquals(port, ArticleConstants.SERVICE_DEFAULT_PORT );

        int fakePort = 99669966;
        config.put(ArticleConstants.CLIENT_OPTION_PORT, fakePort);
        String casenames[] = new String[]{"dev", "prod", "local", "foo", "99669966"};
        List<String> cases = Arrays.asList(casenames);
        for (String env: cases) {
            config.put(ArticleConstants.CLIENT_OPTION_ENV, env);
            port=ac.getPortForClient(config);
            assertEquals(port, fakePort );
        }
    }

    @Test
    public void testSubordinateHostPortConfigs() throws Exception {
        String testConfigFilename = "/test-config.json";
        URL fp = this.getClass().getResource(testConfigFilename);
        InputStream jsonStream = fp.openStream();
        String jsonStr = IOUtils.toString(jsonStream, "UTF-8");
        JsonObject config = new JsonObject(jsonStr);
        JsonObject serviceConfig = ArticleConstants.getServiceConfig(config);
        JsonObject authorConfig = ArticleConstants.getComponentConfig(serviceConfig, ArticleConstants.JSON_AUTHOR_CONFIG_PROPERTY);
        JsonObject dateConfig = ArticleConstants.getComponentConfig(serviceConfig, ArticleConstants.JSON_DATE_CONFIG_PROPERTY);

        // Properties of interest
        String propHost = "defaultHost";
        String propPort = "defaultPort";

        // expected results
        String authorHost="author-dev-main.aws.trendkite.com";
        String dateHost="dateextraction-dev-main.aws.trendkite.com";
        int authorPort=8080;
        int datePort=80;

        assertEquals(authorConfig.getString(propHost), authorHost);
        assertEquals(dateConfig.getString(propHost), dateHost);
        assert(authorConfig.getInteger(propPort) == authorPort);
        assert(dateConfig.getInteger(propPort) == datePort);

        HttpClientOptions authorOptions = new HttpClientOptions(authorConfig);
        HttpClientOptions dateOptions = new HttpClientOptions(dateConfig);

        assertEquals(authorOptions.getDefaultHost(), authorHost);
        assertEquals(dateOptions.getDefaultHost(), dateHost);

        assert(authorOptions.getDefaultPort() == authorPort);
        assert(dateOptions.getDefaultPort() == datePort);
    }
}